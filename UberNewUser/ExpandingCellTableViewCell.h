//
//  ExpandingCellTableViewCell.h
//  SwiftBack
//
//  Created by Hengyu Lim on 11/17/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpandingCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *ltextLabel;
@property (strong, nonatomic) IBOutlet UILabel *calculationLabel;
@property (strong, nonatomic) IBOutlet UILabel *fruitLabel;
@property (strong, nonatomic) IBOutlet UILabel *calcLabel;

@end
