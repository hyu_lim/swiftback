//
//  PickUpVC.m
//  UberNewUser
//
//  Developed by Elluminati on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "PickUpVC.h"
#import "SWRevealViewController.h"
#import "AFNHelper.h"
#import "AboutVC.h"
#import "ContactUsVC.h"
#import "ProviderDetailsVC.h"
#import "CarTypeCell.h"
#import "UIImageView+Download.h"
#import "CarTypeDataModal.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UberStyleGuide.h"
#import "EastimateFareVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "DisplayCardVC.h"
#import <GoogleMaps/GoogleMaps.h>

@interface PickUpVC ()
{
    NSString *strForUserId,*strForUserToken,*strForLatitude,*strForLongitude,*strForRequestID,*strForDriverLatitude,*strForDriverLongitude,*strForTypeid,*strMinFare,*strPassCap,*strETA,*Referral,*dist_price,*time_price,*driver_id,*strDestLongitude,*strDestLatitude,*temp,*gender,*distance,*strForSourceAddress,*strForDestinationAddress;
    NSMutableArray *arrForInformation,*arrForApplicationType,*arrForAddress,*arrDriver,*arrType,*arrForCards;
    NSMutableDictionary *driverInfo;
    GMSMapView *mapView_;
    GMSMarker *source_marker,*destination_marker,*client_marker;
    BOOL is_paymetCard,is_Fare,is_PartialMatch;
    
    NSInteger isDest,futureRequest;
    
    float mapZoom;
}

@end

@implementation PickUpVC
@synthesize timerForCheckReqStatus,timerForRequestInProgress;

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
    [self customSetup];
    
    
    isDest = 1;
    //checks if user has card registered
    arrForCards=[[NSMutableArray alloc]init];
    arrPaymentTypes = [[NSMutableArray alloc]init];
    Referral=@"";
    strForTypeid=@"0";
    self.btnCancel.hidden=YES;
    arrForAddress=[[NSMutableArray alloc]init];
    self.tableForCity.hidden=YES;
    self.viewForPreferral.hidden=YES;
    self.viewForReferralError.hidden=YES;
    is_Fare=NO;
    temp=@"nil";
    driverInfo=[[NSMutableDictionary alloc] init];
    // NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    self.viewForDriver.hidden=YES;
    [self.img_driver_profile applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    
    [self getPaymentTypes];
    
    
    [self.RLDatePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    
    SEL selector = NSSelectorFromString(@"setHighlightsToday:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:self.RLDatePicker];
    
    
    self.viewETA.hidden=YES;
    self.paymentView.hidden=YES;
    [self customFont];
    [self updateLocationManagerr];
    [self setTimerToCheckDriverStatus];
    CLLocationCoordinate2D coordinate = [self getLocation];
    strForCurLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    strForCurLongitude= [NSString stringWithFormat:@"%f", coordinate.longitude];
    strForLatitude=strForCurLatitude;
    strForLongitude=strForCurLongitude;
    [self getAddress];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue] zoom:10.0f];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.viewGoogleMap.frame.size.width, self.viewGoogleMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    mapView_.delegate=self;
    
    [self.viewGoogleMap addSubview:mapView_];
    [self.view bringSubviewToFront:self.tableForCity];
    [self myLocationPressed:self];
    
    [self.RLDatePicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [self.RLDatePicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    
    [self.RLDatePicker addTarget:self
                          action:@selector(setMinimumDateForFutureRequest)
                forControlEvents:UIControlEventValueChanged];
    
    
    
    //Swipe to reveal menu

    [self.mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    //rectangle background
    self.tableForCity.layer.cornerRadius = 10;
    self.tableForCity.layer.borderWidth = 1;
    self.tableForCity.layer.borderColor = [UIColor whiteColor].CGColor;
    self.tableForCity.layer.shadowRadius = 5.0;
    self.tableForCity.layer.shadowOpacity = 0.4;
    
    //rectangle background
    _advanceBackground.layer.cornerRadius = 10;
    _advanceBackground.layer.borderWidth = 1;
    _advanceBackground.layer.borderColor = [UIColor colorWithRed:59.0/255.0f green:177.0/255.0f blue:156.0/255.0f alpha:1.0].CGColor;
    _advanceBackground.layer.shadowRadius = 5.0;
    _advanceBackground.layer.shadowOpacity = 0.4;
    
    //btn Cancel
    _btnCancel.layer.cornerRadius = 5;
    _btnCancel.layer.borderWidth = 1;
    _btnCancel.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //cancel background
    _backgroundCancel.layer.cornerRadius = 10;
    //_backgroundCancel.layer.borderWidth = 1;
    //_backgroundCancel.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundCancel.layer.shadowRadius = 5.0;
    _backgroundCancel.layer.shadowOpacity = 0.4;
    
    
    //female switch attribute
    
    _femaleSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
    
    //rectangle background
//    _backgroundRect.layer.cornerRadius = 10;
//    _backgroundRect.layer.borderWidth = 1;
//    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
//    _backgroundRect.layer.shadowRadius = 5.0;
//    _backgroundRect.layer.shadowOpacity = 0.4;
    
    [self.txtNotes.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.txtNotes.layer setBorderWidth:1.0];
    [self.txtNotes.layer setCornerRadius:4.0];
    [self.txtNotes setTintColor:[UIColor whiteColor]];
    
    
    
    _paymentSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    
    
    self.positionForPassengerView = self.viewForAdditionalPassenger.frame;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    
    
    [self.view layoutIfNeeded];
    
    [self signupForQuickBlox];

}

-(void)reloadDatePicker
{
    [self.RLDatePicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [self.RLDatePicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    [self.RLDatePicker reloadInputViews];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    if ([self.txtNotes isEditing]) {
        [self.viewForAdditionalPassenger setFrame:CGRectMake(0, self.view.frame.size.height - keyboardSize.height - self.viewForAdditionalPassenger.frame.size.height, self.viewForAdditionalPassenger.frame.size.width, self.viewForAdditionalPassenger.frame.size.height)];
    }
    //Given size may not account for screen rotation
    
    
    //your other code here..........
}

-(void)setMinimumDateForFutureRequest
{
    [self.RLDatePicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [self.RLDatePicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    [self.RLDatePicker reloadInputViews];
    
}

-(void)getAssociatedWalkers{
    
    
    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
    [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
    [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
    
    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [helper getDataFromPath:FILE_GET_WALKERS withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (response) {
            
            if ([[response valueForKey:@"success"] boolValue]) {
                
                NSArray *tempWalkers = [response valueForKey:@"walkers"];
                if ([tempWalkers  count] > 0) {
                    
                    arrWalkers = tempWalkers;
                    
                    if (!isQuickbloxConnected) {
                        [self loginForQuickBlox];

                    }
                    [self.chatBtn setHidden:NO];
                }
                else{
                    [self.chatBtn setHidden:YES];
                }
            }
            else{
                [self.chatBtn setHidden:YES];
                if ([response valueForKey:@"error"]) {
                    
                    if ([[response valueForKey:@"error"] isEqualToString:@"Not a valid token"]) {
                        
                        [USERDEFAULT setBool:NO forKey:PREF_IS_LOGIN];
                        [USERDEFAULT removeObjectForKey:PREF_LOGIN_OBJECT];
                        [USERDEFAULT synchronize];
                         [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];

                    }
                }
               // [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
                
            }
        }
        else{
            
        }
    }];
}

-(void)signupForQuickBlox{
    
    NSDictionary *dictLogin = [USERDEFAULT objectForKey:PREF_LOGIN_OBJECT];
    
    NSString *fullname = [NSString stringWithFormat:@"%@ %@",[dictLogin objectForKey:PARAM_FIRST_NAME],[dictLogin objectForKey:PARAM_LAST_NAME]];
    NSString *login = [NSString stringWithFormat:@"%@%@",[dictLogin objectForKey:PARAM_FIRST_NAME],[dictLogin objectForKey:PARAM_LAST_NAME]];

    
    QBUUser *user = [QBUUser user];
    user.login = login;
    user.password = [login stringByAppendingFormat:@"%@",[dictLogin objectForKey:PARAM_ID]];
    user.email = [NSString stringWithFormat:@"%@",[dictLogin objectForKey:PARAM_EMAIL]];
    user.fullName = fullname;

    [user setTags:[NSMutableArray arrayWithObjects:@"Swiftback", nil]];
    
	time++;
    // Registration/sign up of User
    [QBRequest signUp:user successBlock:^(QBResponse *response, QBUUser *user) {
        // Sign up was successful
        currentUser = user;
        [self getAssociatedWalkers];

    } errorBlock:^(QBResponse *response) {
        // Handle error here
        [self getAssociatedWalkers];

    }];
}

-(void)loginForQuickBlox{
    
    NSDictionary *dictLogin = [USERDEFAULT objectForKey:PREF_LOGIN_OBJECT];
    
    NSString *fullname = [NSString stringWithFormat:@"%@ %@",[dictLogin objectForKey:PARAM_FIRST_NAME],[dictLogin objectForKey:PARAM_LAST_NAME]];
    NSString *login = [NSString stringWithFormat:@"%@%@",[dictLogin objectForKey:PARAM_FIRST_NAME],[dictLogin objectForKey:PARAM_LAST_NAME]];

    
    if (currentUser == nil) {
         currentUser = [QBUUser user];
        [currentUser setEmail:[dictLogin objectForKey:PARAM_EMAIL]];
        [currentUser setPassword:[login stringByAppendingFormat:@"%@",[dictLogin objectForKey:PARAM_ID]]];


    }
    [[QMServicesManager instance] logInWithUser:currentUser completion:^(BOOL success, NSString *errorMessage) {
        if (success) {
            
                     // connect to Chat
            [[QBChat instance] connectWithUser:currentUser completion:^(NSError * _Nullable error) {
                
                NSString *stringCheck = [error.userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
                if ([stringCheck isEqualToString:@"You are already connected to chat."] || !error) {
                    
                    isQuickbloxConnected = YES;
                }
                
            }];

        }
        else{
			
			if (time < 3) {
				[self signupForQuickBlox];

			}
        }
    }];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    //    passengerCount = 1;
    DistanceETACount = 0;
    [self.lblNumOfPassenger setText:[NSString stringWithFormat:@"Number of Passenger :  %d",(int)_stepper.value]];
    [self.editBtn setUserInteractionEnabled:NO];
    [self.confirmBtn setUserInteractionEnabled:NO];
    
    if ([USERDEFAULT boolForKey:@"resetFutureReq"]) {
        [USERDEFAULT setBool:NO forKey:@"resetFutureReq"];
        [USERDEFAULT synchronize];
        [mapView_ clear];
        [self.viewForMarker setHidden:NO];
        isDest = 1;
        [self.txtDestination setText:@""];
    }
    mapZoom = mapView_.camera.zoom;
    
    [self.paymodeModeBtn setTitle:@"Select payment mode" forState:UIControlStateNormal];
    
    
    [APPDELEGATE showLoadingWithTitle:@"Please wait"];
    self.viewForReferralError.hidden=YES;
    self.viewForAddress.hidden=NO;
    arrForApplicationType=[[NSMutableArray alloc]init];
    self.viewForDriver.hidden=YES;
    gender=@"male";
    self.viewForMarker.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-40);
    //   NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    // if([[pref valueForKey:PREF_IS_REFEREE] boolValue])
    // {
    self.navigationController.navigationBarHidden=YES;
    self.viewForAcceptRequest.hidden=YES;
    [self getAllApplicationType];
    [super setNavBarTitle:TITLE_PICKUP];
    [self checkForAppStatus];
    [self getPagesData];
    [self.paymentView setHidden:YES];
    self.viewForRequest.hidden=YES;
    self.blurViewForRequest.hidden = YES;
    
    isQuickbloxConnected = NO;
    if(is_Fare==NO)
    {
        self.viewETA.hidden=YES;
        self.viewForFareAddress.hidden=YES;
        [self getProviders];
    }
    else
    {
        self.viewETA.hidden=NO;
        self.viewForFareAddress.hidden=YES;
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        self.lblFareAddress.text=[pref valueForKey:PRFE_FARE_ADDRESS];
        self.lblFare.text=[NSString stringWithFormat:@"$ %@",[pref valueForKey:PREF_FARE_AMOUNT]];
        
        [self.btnFare setTitle:[NSString stringWithFormat:@"%@",[pref valueForKey:PRFE_FARE_ADDRESS]] forState:UIControlStateNormal];
        self.btnFare.titleLabel.numberOfLines=2;
        self.btnFare.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    }
    [self cashBtnPressed:nil];
    [self myLocationPressed:self];
    // }
    // [self performSelector:@selector(showMapCurrentLocatinn) withObject:nil afterDelay:2.5];
    //    NSString *reqID = [USERDEFAULT objectForKey:PREF_REQ_ID];
    //    if (reqID)
    //    {
    //        [self.viewForCancel setHidden:NO];
    //        [self.btnCancel setHidden:NO];
    //
    //    }
    //    NSString *reqID = [USERDEFAULT objectForKey:PREF_REQ_ID];
    //    if (reqID)
    //    {
    //        [self.viewForCancel setHidden:NO];
    //        [self.btnCancel setHidden:NO];
    //    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSString *reqID = [USERDEFAULT objectForKey:PREF_REQ_ID];
    if (!reqID)
    {
        [self.viewForCancel setHidden:YES];
        [self.btnCancel setHidden:YES];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.viewETA.hidden=YES;
    is_Fare=NO;
    self.viewForFareAddress.hidden=YES;
    self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
    [_txtAddress resignFirstResponder];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
        [self.lblNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void)SetLocalization
{
    [self.btnPickMeUp setTitle:NSLocalizedString(@"PICK ME UP", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateSelected];
    // [self.btnClose setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    // [self.btnClose setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateSelected];
    [self.btnPayCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnPayCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnPayRequest setTitle:NSLocalizedString(@"Request", nil) forState:UIControlStateNormal];
    [self.btnPayRequest setTitle:NSLocalizedString(@"Request", nil) forState:UIControlStateSelected];
    [self.btnSelService setTitle:NSLocalizedString(@"SELECT SERVICE YOU NEED", nil) forState:UIControlStateNormal];
    [self.btnSelService setTitle:NSLocalizedString(@"SELECT SERVICE YOU NEED", nil) forState:UIControlStateSelected];
    [self.bReferralSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateNormal];
    [self.bReferralSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateSelected];
    [self.bReferralSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateNormal];
    [self.bReferralSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateSelected];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnRatecard setTitle:NSLocalizedString(@"RATE CARD", nil) forState:UIControlStateNormal];
    [self.btnRatecard setTitle:NSLocalizedString(@"RATE CARD", nil) forState:UIControlStateSelected];
    
    self.lETA.text=NSLocalizedString(@"ETA", nil);
    self.lMaxSize.text=NSLocalizedString(@"MAX SIZE", nil);
    self.lMinFare.text=NSLocalizedString(@"MIN FARE", nil);
    self.lSelectPayment.text=NSLocalizedString(@"Select Your Payment Type", nil);
    self.lRefralMsg.text=NSLocalizedString(@"Referral_Msg", nil);
    self.lRate_basePrice.text=NSLocalizedString(@"Base Price :", nil);
    self.lRate_distancecost.text=NSLocalizedString(@"Distance Cost :", nil);
    self.lRate_TimeCost.text=NSLocalizedString(@"Time Cost :", nil);
    self.lblRateCradNote.text=NSLocalizedString(@"Rate_card_note", nil);
    self.txtAddress.placeholder=NSLocalizedString(@"SEARCH", nil);
    self.txtPreferral.placeholder=NSLocalizedString(@"Enter Referral Code", nil);
}
#pragma mark-
#pragma mark-

-(void)customFont
{
    self.txtAddress.font=[UberStyleGuide fontRegular];
    self.txtDestination.font=[UberStyleGuide fontRegular];
    //self.btnCancel=[APPDELEGATE setBoldFontDiscriptor:self.btnCancel];
    //self.btnPickMeUp=[APPDELEGATE setBoldFontDiscriptor:self.btnPickMeUp];
    self.btnSelService=[APPDELEGATE setBoldFontDiscriptor:self.btnSelService];
}

#pragma mark -
#pragma mark - Location Delegate

-(CLLocationCoordinate2D) getLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

-(void)updateLocationManagerr
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    strForCurLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strForCurLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==40)
    {
        if( buttonIndex == 1 ) /* NO = 0, YES = 1 */
        {
            [self performSegueWithIdentifier:@"segueToPayment" sender:self];
        }
    }
    if(alertView.tag==60)
    {
        if( buttonIndex == 1 ) /* NO = 0, YES = 1 */
        {
            [self performSegueWithIdentifier:@"segueToPayment" sender:self];
        }
    }
    if(alertView.tag==20)
    {
        if( buttonIndex == 1 ) /* NO = 0, YES = 1 */
        {
            [self reloadDatePicker];
            [self.viewForDatePick setHidden:NO];
            
        }
    }
    
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }
    }
    if (alertView.tag == 9)
    {
        if (buttonIndex == 0)
        {
            [self.viewForRequest setHidden:NO];
            [self.blurViewForRequest setHidden:NO];
            [self.viewForConfirmRequest setHidden:YES];
            [self.viewForConfirmRequestTwo setHidden:YES];
            [self.viewForAddress setHidden:NO];
            [self.editBtn setUserInteractionEnabled:NO];
            [self.confirmBtn setUserInteractionEnabled:NO];
        }
    }
    
    
}


#pragma mark- Google Map Delegate

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    
    
    if (isDest == 1) {
        
        if (mapView_.camera.zoom == mapZoom) {
            strForLatitude=[NSString stringWithFormat:@"%f",position.target.latitude];
            strForLongitude=[NSString stringWithFormat:@"%f",position.target.longitude];
        }
        else{
          
            mapZoom = mapView_.camera.zoom;
        }
        
    }
    else if (isDest == 2){
    }
    
}

- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if (isList) {
        isList = NO;
    }
    else{
        [self getAddress];
        [self getProviders];
        
    }
}

-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    if (gesture && isDest != 3) {
        isDest = 1;
        [self.txtDestination setText:@""];
        [self.viewForRequest setHidden:YES];
        [self.blurViewForRequest setHidden: YES];
    }
}

-(void)getAddress
{
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: nil];
    
    NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"end_address"];
    if (getAddress.count!=0)
    {
        if (isDest == 1) {
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
            
        }
        else{
        }
    }
    
}
-(void)getAddress:(NSString *)type
{
    if([type isEqualToString:@"source"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        // NSArray *getLocation = [getLegs valueForKey:@"end_location"];
        
        //  srclat = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lat"] doubleValue];
        
        //  srclong = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lng"] doubleValue];
        
        if (getAddress.count!=0)
        {
            if (isDest == 1) {
            }
            else{
                self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
                
            }
        }
        [mapView_ clear];
        //        client_marker.map=nil;
        //        client_marker = [[GMSMarker alloc] init];
        //        client_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
        //        client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
        //        client_marker.map = mapView_;
        [self getProviders];
        // strSourceAddress = self.txtAddress.text;
    }
    if([type isEqualToString:@"destination"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strDestLatitude              floatValue], [strDestLongitude floatValue], [strDestLatitude floatValue], [strDestLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        // NSArray *getLocation = [getLegs valueForKey:@"end_location"];
        
        // destlat = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lat"] doubleValue];
        
        // destlong = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lng"] doubleValue];
        
        
        
        if (getAddress.count!=0)
        {
            self.txtDestination.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
        // strDestinationAddress = self.txtDestination.text;
        
    }
    if ([type isEqualToString:@"Current"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForCurLatitude floatValue], [strForCurLongitude floatValue], [strForCurLatitude floatValue], [strForCurLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        
        if(getAddress.count != 0)
        {
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
        // strSourceAddress = self.txtAddress.text;
    }
}

#pragma mark -
#pragma mark - Mapview Delegate

-(void)showMapCurrentLocatinn
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:14];
    [mapView_ animateWithCameraUpdate:updatedCamera];
    
    [self getAddress];
}


#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark- Searching Method

- (IBAction)Searching:(id)sender
{
    aPlacemark=nil;
    temp=@"source";
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    //CLGeocoder *geocoder;
    
    NSString *str=self.txtAddress.text;
    NSLog(@"%@",str);
    
    
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 self.tableForCity.hidden=NO;
                 
                 placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark]; o
                 [self.tableForCity reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     self.tableForCity.hidden=YES;
                 }
             }
             
         }
         
     }];
    
}
- (IBAction)DestinationSearching:(id)sender
{
    aPlacemark=nil;
    temp=@"destination";
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    //  CLGeocoder *geocoder;
    
    NSString *str=self.txtDestination.text;
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 self.tableForCity.hidden=NO;
                 
                 placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark]; o
                 [self.tableForCity reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     self.tableForCity.hidden=YES;
                 }
             }
             
         }
         
     }];
    
    
}
#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    
    if([placeMarkArr count]>0)
    {
        NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
        
        // cell.lblTitle.text=currentPlaceMark.name;
        cell.textLabel.text=formatedAddress;
        
        cell.textLabel.font=[UberStyleGuide fontRegular];
        
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    self.tableForCity.hidden=YES;
    // [self textFieldShouldReturn:nil];
    
    [self setNewPlaceData];
    
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(void)setNewPlaceData
{
    if([temp isEqualToString:@"source"])
    {
        isList = YES;
        placeID = [aPlacemark objectForKey:@"place_id"];
        if ([[aPlacemark objectForKey:@"partial_match"]boolValue])
        {
            is_PartialMatch=YES;
        }
        else
        {
            is_PartialMatch=NO;
        }
        self.txtAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtAddress];
    }
    else if ([temp isEqualToString:@"destination"])
    {
        placeID = [aPlacemark objectForKey:@"place_id"];
        isList = YES;
        if ([[aPlacemark objectForKey:@"partial_match"]boolValue])
        {
            is_PartialMatch=YES;
        }
        else
        {
            is_PartialMatch=NO;
        }
        self.txtDestination.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtDestination];
    }
}


#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_ABOUT])
    {
        AboutVC *obj=[segue destinationViewController];
        obj.arrInformation=arrForInformation;
    }
    else if([segue.identifier isEqualToString:SEGUE_TO_ACCEPT])
    {
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
        obj.strForDistance=self.lblDistance.text;
        obj.strForPrice=self.lblPrice.text;
    }
    else if([segue.identifier isEqualToString:@"contactus"])
    {
        ContactUsVC *obj=[segue destinationViewController];
        obj.dictContent=sender;
    }
    else if ([segue.identifier isEqualToString:@"segueToEastimate"])
    {
        EastimateFareVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strMinFare=strMinFare;
    }
}

-(void)goToSetting:(NSString *)str
{
    [self performSegueWithIdentifier:str sender:self];
}

-(void)rideNext
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        if([[pref valueForKey:PREF_IS_STARTED] boolValue])
        {
            if ([vc isKindOfClass:[DisplayCardVC class]])
            {
                obj = (DisplayCardVC *)vc;
            }
        }
        else if ([vc isKindOfClass:[ProviderDetailsVC class]])
        {
            obj = (ProviderDetailsVC *)vc;
        }
        else if ([vc isKindOfClass:[PickUpVC class]])
        {
            obj = (PickUpVC *)vc;
        }
        
    }
    [self.navigationController popToViewController:obj animated:NO];
    
    
}
-(void)goToRide
{
    // [self.navigationController popToRootViewControllerAnimated:YES];
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        if([[pref valueForKey:PREF_IS_STARTED] boolValue])
        {
            if ([vc isKindOfClass:[DisplayCardVC class]])
            {
                obj = (DisplayCardVC *)vc;
            }
        }
        else if ([vc isKindOfClass:[ProviderDetailsVC class]])
        {
            obj = (ProviderDetailsVC *)vc;
        }
        else if ([vc isKindOfClass:[PickUpVC class]])
        {
            obj = (PickUpVC *)vc;
        }
        
    }
    [self.navigationController popToViewController:obj animated:NO];
}

#pragma mark -
#pragma mark - UIButton Action

- (IBAction)eastimateFareBtnPressed:(id)sender
{
    is_Fare=YES;
    self.viewForRateCard.hidden=YES;
    [self performSegueWithIdentifier:@"segueToEastimate" sender:nil];
}

- (IBAction)closeETABtnPressed:(id)sender
{
    self.viewETA.hidden=YES;
    self.viewForFareAddress.hidden=YES;
    self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
    is_Fare=NO;
}

- (IBAction)RateCardBtnPressed:(id)sender
{
    self.viewForRateCard.hidden=NO;
}


- (IBAction)ETABtnPressed:(id)sender {
    
    self.viewETA.hidden=NO;
    self.viewForRateCard.hidden=YES;
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateSelected];
}

- (IBAction)cashBtnPressed:(id)sender
{
    [self.btnCash setSelected:YES];
    [self.btnCard setSelected:NO];
    is_paymetCard=NO;
}

- (IBAction)cardBtnPressed:(id)sender
{
    [self.btnCash setSelected:NO];
    [self.btnCard setSelected:YES];
    is_paymetCard=YES;
}

- (IBAction)requestBtnPressed:(id)sender
{
    
    if ([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Select payment mode"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select payment mode" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        if ([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Pay via Card"]) {
            if([arrForCards count] > 0){
                [self createRideNowRequest];
                
            }
            else{
                
                
                UIAlertView *alertPayment2 = [[UIAlertView alloc]initWithTitle:@"Add Payment"
                                              
                                                                       message:@"Oops, you have to add payment to make a request, would you like to add a card now? "
                                              
                                                                      delegate:self
                                              
                                                             cancelButtonTitle:@"No"
                                              
                                                             otherButtonTitles:@"Yes",nil];
                
                alertPayment2.tag=60;
                [alertPayment2 show];
            }
            
        }
        else
        {
            [self createRideNowRequest];
        }
        
    }
    
    
}

-(void)createRideNowRequest{
    if([CLLocationManager locationServicesEnabled])
    {
        if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
        {
            strForTypeid=@"1";
        }
        if(![strForTypeid isEqualToString:@"0"])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else
            {
                if([[AppDelegate sharedAppDelegate]connected])
                {
                    
                    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    
                    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
                    //[dictParam setValue:@"22.3023117"  forKey:PARAM_LATITUDE];
                    //[dictParam setValue:@"70.7969645"  forKey:PARAM_LONGITUDE];
                    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                    [dictParam setValue:strForUserId forKey:PARAM_ID];
                    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
                    if (is_paymetCard)
                    {
                        [dictParam setValue:@"0" forKey:PARAM_PAYMENT_MODE];
                    }
                    else
                    {
                        [dictParam setValue:@"1" forKey:PARAM_PAYMENT_MODE];
                    }
                    
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         
                         if (response)
                         {
                             self.paymentView.hidden=YES;
                             if([[response valueForKey:@"success"]boolValue])
                             {
                                 NSLog(@"pick up......%@",response);
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                     [self showDriver:walker];
                                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                     
                                     strForRequestID=[response valueForKey:@"request_id"];
                                     [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                     [pref synchronize];
                                     [self setTimerToCheckDriverStatus];
                                     
                                     [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                     [self.btnCancel setHidden:NO];
                                     [self.viewForDriver setHidden:NO];
                                     [APPDELEGATE.window addSubview:self.btnCancel];
                                     [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                     [APPDELEGATE.window addSubview:self.viewForDriver];
                                     [APPDELEGATE.window bringSubviewToFront:self.viewForDriver];
                                 }
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                                 
                             }
                         }
                         
                         
                     }];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                }
            }
            
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
        
        
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
    
}

- (IBAction)onClickSwitch:(id)sender
{
    
    if (self.femaleSwitch.isOn)
    {
        gender=@"female";
    }
    else
    {
        gender=@"male";
    }
    //    UIButton *btn=(UIButton *)sender;
    //    if (btn.tag==0)
    //    {
    //         btn.tag=1;
    //        [btn setBackgroundImage:[UIImage imageNamed:@"switch_on"] forState:UIControlStateNormal];
    //        gender=@"female";
    //    }
    //    else
    //    {
    //        btn.tag=0;
    //        [btn setBackgroundImage:[UIImage imageNamed:@"switch_off"] forState:UIControlStateNormal];
    //        gender=@"male";
    //    }
}

- (IBAction)onClickRideLater:(id)sender {
    
    if ([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Select payment mode"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select payment mode" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if ([strDistance integerValue] > 0)
        {
            if ([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Select payment mode"])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select payment mode" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Pay via Card"]){
                
                if ([arrForCards count] > 0) {
                    [self reloadDatePicker];
                    [self.viewForDatePick setHidden:NO];
                    
                }
                else{
                    //jam
                    UIAlertView *alertPayment2 = [[UIAlertView alloc]initWithTitle:@"Add Payment"
                                                  
                                                                           message:@"Oops, you have to add payment to make a request, would you like to add a card now? "
                                                  
                                                                          delegate:self
                                                  
                                                                 cancelButtonTitle:@"No"
                                                  
                                                                 otherButtonTitles:@"Yes",nil];
                    
                    alertPayment2.tag=60;
                    [alertPayment2 show];
                }
            }
            else{
                [self reloadDatePicker];
                [self.viewForDatePick setHidden:NO];
                
            }
            
            
        }
        else
        {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
        }
    }
    
}
- (IBAction)cancelBtnPressed:(id)sender
{
    [self.paymentView setHidden:YES];
}



- (IBAction)pickMeUpBtnPressed:(id)sender
{
    if ([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Select payment mode"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select payment mode" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        if ([self.paymodeModeBtn.titleLabel.text isEqualToString:@"Pay via Card"]) {
            
            if ([arrForCards count] > 0) {
                
                //[self.paymentView setHidden:NO];
                if([CLLocationManager locationServicesEnabled])
                {
                    if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
                    {
                        strForTypeid=@"1";
                    }
                    if(![strForTypeid isEqualToString:@"0"])
                    {
                        if(((strForLatitude==nil)&&(strForLongitude==nil))
                           ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
                        {
                            [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
                        }
                        else
                        {
                            if([[AppDelegate sharedAppDelegate]connected])
                            {
                                
                                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                                
                                NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                strForUserId=[pref objectForKey:PREF_USER_ID];
                                strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                                
                                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                                [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                                [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
                                [dictParam setValue:strDestLatitude forKey:@"d_latitude"];
                                [dictParam setValue:strDestLongitude forKey:@"d_longitude"];
                                [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                                [dictParam setValue:strForUserId forKey:PARAM_ID];
                                [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                                [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
                                [dictParam setValue:@"0" forKey:@"payment_mode"];
                                [dictParam setValue:gender forKey:@"gender"];
                                [dictParam setValue:self.txtAddress.text forKey:@"s_address"];
                                [dictParam setValue:self.txtDestination.text forKey:@"d_address"];
                                if (is_paymetCard)
                                {
                                    [dictParam setValue:@"0" forKey:PARAM_PAYMENT_MODE];
                                }
                                else
                                {
                                    [dictParam setValue:@"1" forKey:PARAM_PAYMENT_MODE];
                                }
                                
                                
                                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                                [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                                 {
                                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                                     
                                     if (response)
                                     {
                                         self.paymentView.hidden=YES;
                                         if([[response valueForKey:@"success"]boolValue])
                                         {
                                             NSLog(@"pick up......%@",response);
                                             if([[response valueForKey:@"success"]boolValue])
                                             {
                                                 self.viewForAcceptRequest.hidden=NO;
                                                 
                                                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                                                 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                                 
                                                 strForRequestID=[response valueForKey:@"request_id"];
                                                 [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                                 [pref synchronize];
                                                 [timerForRequestInProgress invalidate];
                                                 timerForRequestInProgress=nil;
                                                 timerForCheckReqStatus=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
                                                 [self checkForRequestStatus];
                                                 
                                                 self.viewForRequest.hidden=YES;
                                                 self.blurViewForRequest.hidden = YES;
                                                 [self.viewForCancel setHidden:NO];
                                                 [self.btnCancel setHidden:NO];
                                                 // self.viewForAcceptRequest.hidden=NO;
                                                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                     self.viewForAcceptRequest.hidden=YES;
                                                     NSLog(@"Reached");
                                                 });
                                                 NSLog(@"REACHED");
                                             }
                                         }
                                         else
                                         {
                                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                             [alert show];
                                             if([[response valueForKey:@"error"] isEqualToString:@"Previous Request is Pending"])
                                             {
                                                 self.viewForAcceptRequest.hidden=YES;
                                                 [timerForRequestInProgress invalidate];
                                                 timerForRequestInProgress=nil;
                                                 timerForRequestInProgress=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkRequestInProgress) userInfo:nil repeats:YES];
                                             }
                                             
                                         }
                                     }
                                     else
                                     {
                                         
                                     }
                                     
                                 }];
                            }
                            else
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                                [alert show];
                            }
                        }
                        
                    }
                    else
                        [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
                }
                else
                {
                    UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    alertLocation.tag=100;
                    [alertLocation show];
                    
                }
                
            }else{
                UIAlertView *alertPayment2 = [[UIAlertView alloc]initWithTitle:@"Add Payment"
                                              
                                                                       message:@"Oops, you have to add payment to make a request, would you like to add a card now? "
                                              
                                                                      delegate:self
                                              
                                                             cancelButtonTitle:@"No"
                                              
                                                             otherButtonTitles:@"Yes",nil];
                
                alertPayment2.tag=60;
                [alertPayment2 show];
            }
            
        }
        else{
            
            //[self.paymentView setHidden:NO];
            if([CLLocationManager locationServicesEnabled])
            {
                if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
                {
                    strForTypeid=@"1";
                }
                if(![strForTypeid isEqualToString:@"0"])
                {
                    if(((strForLatitude==nil)&&(strForLongitude==nil))
                       ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
                    {
                        [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
                    }
                    else
                    {
                        if([[AppDelegate sharedAppDelegate]connected])
                        {
                            
                            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                            
                            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                            strForUserId=[pref objectForKey:PREF_USER_ID];
                            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                            
                            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                            [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                            [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
                            [dictParam setValue:strDestLatitude forKey:@"d_latitude"];
                            [dictParam setValue:strDestLongitude forKey:@"d_longitude"];
                            [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                            [dictParam setValue:strForUserId forKey:PARAM_ID];
                            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                            [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
                            [dictParam setValue:@"0" forKey:@"payment_mode"];
                            [dictParam setValue:gender forKey:@"gender"];
                            [dictParam setValue:self.txtAddress.text forKey:@"s_address"];
                            [dictParam setValue:self.txtDestination.text forKey:@"d_address"];
                            if (is_paymetCard)
                            {
                                [dictParam setValue:@"0" forKey:PARAM_PAYMENT_MODE];
                            }
                            else
                            {
                                [dictParam setValue:@"1" forKey:PARAM_PAYMENT_MODE];
                            }
                            
                            
                            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                            [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                             {
                                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                                 
                                 if (response)
                                 {
                                     self.paymentView.hidden=YES;
                                     if([[response valueForKey:@"success"]boolValue])
                                     {
                                         NSLog(@"pick up......%@",response);
                                         if([[response valueForKey:@"success"]boolValue])
                                         {
                                             self.viewForAcceptRequest.hidden=NO;
                                             
                                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                                             NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                             
                                             strForRequestID=[response valueForKey:@"request_id"];
                                             [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                             [pref synchronize];
                                             [timerForRequestInProgress invalidate];
                                             timerForRequestInProgress=nil;
                                             timerForCheckReqStatus=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
                                             [self checkForRequestStatus];
                                             
                                             self.viewForRequest.hidden=YES;
                                             self.blurViewForRequest.hidden = YES;
                                             [self.viewForCancel setHidden:NO];
                                             [self.btnCancel setHidden:NO];
                                             // self.viewForAcceptRequest.hidden=NO;
                                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                 self.viewForAcceptRequest.hidden=YES;
                                                 NSLog(@"Reached");
                                             });
                                             NSLog(@"REACHED");
                                         }
                                     }
                                     else
                                     {
                                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                         [alert show];
                                         if([[response valueForKey:@"error"] isEqualToString:@"Previous Request is Pending"])
                                         {
                                             self.viewForAcceptRequest.hidden=YES;
                                             [timerForRequestInProgress invalidate];
                                             timerForRequestInProgress=nil;
                                             timerForRequestInProgress=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkRequestInProgress) userInfo:nil repeats:YES];
                                         }
                                         
                                     }
                                 }
                                 else
                                 {
                                     
                                 }
                                 
                             }];
                        }
                        else
                        {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                            [alert show];
                        }
                    }
                    
                }
                else
                    [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
            }
            else
            {
                UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                alertLocation.tag=100;
                [alertLocation show];
                
            }
        }
        
        
    }
    
    
}

- (IBAction)cancelReqBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         self.btnCancel.hidden=YES;
                         [self.viewForCancel setHidden:YES];
                         self.viewForDriver.hidden=YES;
                         //[self.btnCancel removeFromSuperview];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         [USERDEFAULT removeObjectForKey:PREF_REQ_ID];
                         [USERDEFAULT synchronize];
                         
                         [mapView_ clear];
                         [self.viewForMarker setHidden:NO];
                         [self.paymodeModeBtn setTitle:@"Select payment mode" forState:UIControlStateNormal];

                     }
                     else
                     {}
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
        
    }
}

- (IBAction)myLocationPressed:(id)sender
{
    if ([CLLocationManager locationServicesEnabled])
    {
        
        //        CLLocationCoordinate2D coordinate = [self getLocation];
        //
        //        strForCurLatitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
        //        strForCurLongitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
        CLLocationCoordinate2D coor;
        coor.latitude=[strForCurLatitude doubleValue];
        coor.longitude=[strForCurLongitude doubleValue];
        
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:strForCurLatitude message:strForCurLongitude delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        //        [alert show];
        
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
        [mapView_  animateWithCameraUpdate:updatedCamera];
        
        [mapView_ clear];
        [self.viewForMarker setHidden:NO];
        [self.viewForRequest setHidden:YES];
        //        client_marker = [[GMSMarker alloc] init];
        //        client_marker.position = CLLocationCoordinate2DMake([strForCurLatitude doubleValue], [strForCurLongitude doubleValue]);
        //        client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
        //        client_marker.map = mapView_;
        [self getAddress:@"Current"];
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
    
}

- (IBAction)selectServiceBtnPressed:(id)sender
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        float closeY=(iOSDeviceScreenSize.height-self.btnSelService.frame.size.height);
        
        float openY=closeY-(self.bottomView.frame.size.height-self.btnSelService.frame.size.height);
        if (self.bottomView.frame.origin.y==closeY)
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, openY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
            
        }
        else
        {
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, closeY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
            } completion:^(BOOL finished)
             {
             }];
            
        }
        
    }
    
    
}

#pragma mark -
#pragma mark - Custom WS Methods

-(void)getAllApplicationType
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_APPLICATION_TYPE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableArray *arr=[[NSMutableArray alloc]init];
                     [arr addObjectsFromArray:[response valueForKey:@"types"]];
                     arrType=[response valueForKey:@"types"];
                     for(NSMutableDictionary *dict in arr)
                     {
                         CarTypeDataModal *obj=[[CarTypeDataModal alloc]init];
                         obj.id_=[dict valueForKey:@"id"];
                         obj.name=[dict valueForKey:@"name"];
                         obj.icon=[dict valueForKey:@"icon"];
                         obj.is_default=[dict valueForKey:@"is_default"];
                         obj.price_per_unit_time=[dict valueForKey:@"price_per_unit_time"];
                         obj.price_per_unit_distance=[dict valueForKey:@"price_per_unit_distance"];
                         obj.base_price=[dict valueForKey:@"base_price"];
                         obj.isSelected=NO;
                         [arrForApplicationType addObject:obj];
                     }
                     [self.collectionView reloadData];
                 }
                 //  [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 
                 else
                 {}
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
}
-(void)setTimerToCheckDriverStatus
{
    if(timerForRequestInProgress)
    {
        [timerForRequestInProgress invalidate];
        timerForRequestInProgress=nil;
    }
    strForRequestID = [USERDEFAULT objectForKey:PREF_REQ_ID];
    if (strForRequestID)
    {
        [self checkForRequestStatus];
    }
    else
    {
        [self checkRequestInProgress];
        timerForRequestInProgress=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkRequestInProgress) userInfo:nil repeats:YES];
        
        
    }
}
-(void)checkForAppStatus
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    //  [pref removeObjectForKey:PREF_REQ_ID];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    if(strReqId!=nil)
    {
        self.viewForAcceptRequest.hidden=NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            self.viewForAcceptRequest.hidden=YES;
        });
        if (timerForCheckReqStatus)
        {
            [timerForCheckReqStatus invalidate];
            timerForCheckReqStatus = nil;
        }
        timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkRequestInProgress) userInfo:nil repeats:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            self.viewForAcceptRequest.hidden=YES;
        });
        //[self checkForRequestStatus];
    }
    else
    {
        [self checkRequestInProgress];
        //[self RequestInProgress];
    }
}
-(void)checkForRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [self getAssociatedWalkers];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     
                     if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                     {
                         self.viewForAcceptRequest.hidden=YES;
                         NSLog(@"GET REQ--->%@",response);
                         NSString *strCheck=[response valueForKey:@"walker"];
                         
                         if(strCheck)
                         {
                             [timerForRequestInProgress invalidate];
                             timerForRequestInProgress=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                             strForDriverLatitude=[dictWalker valueForKey:@"d_latitude"];
                             strForDriverLongitude=[dictWalker valueForKey:@"d_longitude"];
                             strForLatitude=[dictWalker valueForKey:@"latitude"];
                             strForLongitude=[dictWalker valueForKey:@"longitude"];
                             if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                             {
                                 [pref removeObjectForKey:PREF_REQ_ID];
                                 [pref synchronize];
                                 return ;
                             }
                             self.viewForAddress.hidden=YES;
                             
                             ProviderDetailsVC *vcFeed = nil;
                             for (int i=0; i<self.navigationController.viewControllers.count; i++)
                             {
                                 UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                                 if ([vc isKindOfClass:[ProviderDetailsVC class]])
                                 {
                                     vcFeed = (ProviderDetailsVC *)vc;
                                 }
                                 
                             }
                             if (vcFeed==nil)
                             {
                                 [self.viewForCancel setHidden:YES];
                                 [self.btnCancel setHidden:YES];
                                 [timerForCheckReqStatus invalidate];
                                 timerForCheckReqStatus=nil;
                                 
                                 [mapView_ clear];
                                 [self.viewForMarker setHidden:NO];
                                 
                                 [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                                 return;
                             }else
                             {
                                 [self.navigationController popToViewController:vcFeed animated:NO];
                             }
                         }
                         
                     }
                     if ([response valueForKey:@"current_walker"])
                     {
                         if([[response valueForKey:@"current_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                         {
                             //                         self.viewForAcceptRequest.hidden=NO;
                             //
                             //                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                             //                             self.viewForAcceptRequest.hidden=YES;
                             //                         });
                             
                             
                             //  self.txtAddress.text=strForSourceAddress;
                             //  self.txtDestination.text=strForDestinationAddress;
                             
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             
                             [USERDEFAULT removeObjectForKey:PREF_REQ_ID];
                             [USERDEFAULT synchronize];
                             
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                             
                             UIAlertView *alertUnavailable = [[UIAlertView alloc]initWithTitle:@"No Driver Available"
                                                                                       message:@"Seems like drivers around are engaged at the moment. Would you like to make an advance request?"
                                                                                      delegate:self
                                                                             cancelButtonTitle:@"No"
                                                                             otherButtonTitles:@"Yes",nil];
                             alertUnavailable.tag=20;
                             [alertUnavailable show];
                             [mapView_ clear];
                             [self.txtDestination setText:@""];
                             
                             [APPDELEGATE hideLoadingView];
                             self.btnCancel.hidden=YES;
                             self.viewForCancel.hidden=YES;
                             self.viewForDriver.hidden=YES;
                             //  [self showMapCurrentLocatinn];
                             
                         }
                         
                     }
                     //                     else
                     //                     {
                     //                         // driverInfo=[response valueForKey:@"walker"];
                     //                         // [self showDriver:driverInfo];
                     //                     }
                 }
                 else
                 {
                     if([[response valueForKey:@"error"] isEqualToString:@"Passenger Has Cancelled the Request"])
                     {
                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:PREF_REQ_ID];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         self.viewForAcceptRequest.hidden=YES;
                         self.viewForRequest.hidden=YES;
                         self.blurViewForRequest.hidden = YES;
                     }
                 }
             }
             else
             {}
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
/*
 -(void)checkDriverStatus
 {
 if([[AppDelegate sharedAppDelegate]connected])
 {
 // [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
 
 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
 strForUserId=[pref objectForKey:PREF_USER_ID];
 strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
 NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
 
 NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
 
 AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
 [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
 {
 if([[response valueForKey:@"success"]boolValue])
 {
 NSLog(@"GET REQ--->%@",response);
 NSString *strCheck=[response valueForKey:@"walker"];
 
 if(strCheck)
 {
 [timerForCheckReqStatus invalidate];
 timerForCheckReqStatus=nil;
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
 }
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 
 }
 else
 {}
 }];
 }
 else
 {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
 [alert show];
 }
 }*/
-(void)checkRequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     if([[response valueForKey:@"request_id"] integerValue]!= -1)
                     {
                         self.viewForAcceptRequest.hidden=YES;
                         self.viewForRequest.hidden=YES;
                         self.blurViewForRequest.hidden = YES;
                         self.viewForCancel.hidden = NO;
                         self.btnCancel.hidden=NO;
                         [[NSUserDefaults standardUserDefaults]setValue:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         if(timerForRequestInProgress)
                         {
                             self.viewForAcceptRequest.hidden=NO;
                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                 self.viewForAcceptRequest.hidden=YES;
                             });
                             
                             [timerForRequestInProgress invalidate];
                             timerForRequestInProgress=nil;
                         }
                         if (timerForCheckReqStatus)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus = nil;
                         }
                         timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
                         [self checkForRequestStatus];
                     }
                     else
                     {
                         NSString *strReq =[USERDEFAULT  objectForKey:PREF_REQ_ID];
                         if (strReq)
                         {
                             [USERDEFAULT removeObjectForKey:PREF_REQ_ID];
                             [USERDEFAULT synchronize];
                         }
                         
                         [self checkForRequestStatus];
                     }
                 }
                 else
                 {}
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
-(void)RequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     //                     NSMutableDictionary *charge_details=[response valueForKey:@"charge_details"];
                     //                     dist_price=[charge_details valueForKey:@"distance_price"];
                     //                     [pref setObject:dist_price forKey:PRFE_PRICE_PER_DIST];
                     //                     time_price=[charge_details valueForKey:@"price_per_unit_time"];
                     //                     [pref setObject:[charge_details valueForKey:@"price_per_unit_time"] forKey:PRFE_PRICE_PER_TIME];
                     //                     self.lblRate_DistancePrice.text=[NSString stringWithFormat:@"$ %@",dist_price];
                     //                     self.lblRate_TimePrice.text=[NSString stringWithFormat:@"$ %@",time_price];
                     
                     [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     [pref synchronize];
                     [self checkForRequestStatus];
                 }
                 else
                 {}
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getPaymentTypes
{
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_PAYMENT_TYPE withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Payment  = %@",response);
             [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 NSDictionary *dictPayment = [response valueForKey:@"payments"];
                 
                 if ([[dictPayment valueForKey:@"allow_card_payment"]isEqualToString:@"1"])
                 {
                     [arrPaymentTypes addObject:@"Pay via Card"];
                 }
                 
                 if ([[dictPayment valueForKey:@"allow_cash_payment"]isEqualToString:@"1"])
                 {
                     [arrPaymentTypes addObject:@"Pay via Cash"];
                 }
                 
                 for (NSString *type in arrPaymentTypes)
                 {
                     [_paymentSheet addButtonWithTitle:type];
                 }
                 
                 
             }
             
         }];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
    
}

-(void)getPagesData
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@",FILE_PAGE,PARAM_ID,strForUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 arrPage=[response valueForKey:@"informations"];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     //   [APPDELEGATE showToastMessage:@"Requset Accepted"];
                 }
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getProviders
{
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
    [dictParam setValue:strForLatitude forKey:@"latitude"];
    [dictParam setValue:strForLongitude forKey:@"longitude"];
    
    
    //    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GET_PROVIDER", nil)];
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_GET_PROVIDERS withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Get Provider= %@",response);
             // [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 // [arrDriver removeAllObjects];
                 arrDriver=[response valueForKey:@"walkers"];
                 [self showProvider];
                 
                 
             }
             else
             {
                 arrDriver=[[NSMutableArray alloc] init];
                 [self showProvider];
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];        [alert show];
    }
}

-(void)showProvider
{
    if (routes) {
        routes = [[NSArray alloc]init];
    }
    else{
        [mapView_ clear];
        
    }
    
    
    
    //    client_marker = [[GMSMarker alloc] init];
    //    client_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    //    client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
    //    client_marker.map = mapView_;
    BOOL is_first=YES;
    for (int i=0; i<arrDriver.count; i++)
    {
        NSDictionary *dict=[arrDriver objectAtIndex:i];
        //  NSString *strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"type"]];
        // if ([strForTypeid isEqualToString:strType])
        // {
        GMSMarker *driver_marker;
        driver_marker = [[GMSMarker alloc] init];
        driver_marker.position = CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue],[[dict valueForKey:@"longitude"]doubleValue]);
        driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
        driver_marker.map = mapView_;
        driver_marker.opacity = 0.7;
        
        if (is_first)
        {
            [self getETA:dict];
            is_first=NO;
        }
        // }
    }
    is_first=YES;
    
}

-(void)getETA:(NSDictionary *)dict
{
    CLLocationCoordinate2D scorr=CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    CLLocationCoordinate2D dcorr=CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue], [[dict valueForKey:@"longitude"]doubleValue]);
    [self calculateRoutesFrom:scorr to:dcorr];
    
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSLog(@"api url: %@", apiUrl);
    NSError *error;
    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSASCIIStringEncoding error:&error];
    
    NSArray *str1=[apiResponse componentsSeparatedByString:@","];
    if(str1.count>0)
    {
        NSArray *str2=[[str1 objectAtIndex:0] componentsSeparatedByString:@"/"];
        if(str2.count>0)
        {
            
            NSString *setDist=[[str2 objectAtIndex:0] substringFromIndex:1];
            NSArray *arr1=[setDist componentsSeparatedByString:@" "];
            setDist=[[arr1 objectAtIndex:1] substringFromIndex:1];
            
            
            NSString *setTime=[str2 objectAtIndex:1];
            NSString *settime1=[setTime stringByReplacingOccurrencesOfString:@")\"" withString:@" "];
            NSArray *time=[settime1 componentsSeparatedByString:@"}"];
            strETA=[time objectAtIndex:0];
            // strETA=[setTime stringByReplacingOccurrencesOfString:@")\"" withString:@" "];
            
        }
    }
    //NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl];
    
    
    //  NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
    
    return nil;
}
-(NSArray*) calculateRoutesFrom1:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    //chk
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSLog(@"api url: %@", apiUrl);
    NSError *error;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    NSDictionary *getRoutes = [json valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"distance"];
    
    NSString *distance1;
    NSString *dist_type;
    if ([getAddress count]> 0)
    {
        NSArray *data1=[[getAddress objectAtIndex:0]valueForKey:@"text"];
        NSString *str3 = [data1 objectAtIndex:0];
        NSArray *str4=[str3 componentsSeparatedByString:@" "];
        distance1=[str4 objectAtIndex:0];
        dist_type=[str4 objectAtIndex:1];
    }
    
    if ([distance1 length] < 1 || [distance1 isKindOfClass:[NSNull class]] || distance1 == nil)
    {
        distance1 = @"N/A";
        distance = @"N/A";
    }
    
    if([dist_type isEqualToString:@"mi"])
    {
        distance=[NSString stringWithFormat:@"%.2f",[distance1 floatValue]*1.6];
    }
    else if ([dist_type isEqualToString:@"m"])
    {
        
        distance=[NSString stringWithFormat:@"%.2f",[distance1 floatValue]/1000];
    }
    else if ([dist_type isEqualToString:@"km"])
    {
        distance=[NSString stringWithFormat:@"%.2f",[distance1 floatValue]];
    }
    self.lblDistance.text=[NSString stringWithFormat:@"%@ km",distance];
    if([distance intValue]< 32)
    {
        if([distance intValue]<16)
        {
            if([distance intValue]<8)
            {
                self.lblPrice.text=@"$5";
            }
            else
            {
                self.lblPrice.text=@"$10";
            }
        }
        else
        {
            self.lblPrice.text=@"$15";
        }
    }
    else
    {
        self.lblPrice.text=@"$15";
    }
    return nil;
}

-(void)showDriver:(NSMutableDictionary *)walker
{
    if(![driver_id isEqualToString:[walker valueForKey:@"id"]])
    {
        driver_id=[walker valueForKey:@"id"];
        self.lbl_driverName.text=[NSString stringWithFormat:@"%@ %@",[walker valueForKey:@"first_name"],[walker valueForKey:@"last_name"]];
        self.lbl_driverRate.text=[walker valueForKey:@"rating"];
        self.lbl_driver_Carname.text=[walker valueForKey:@"car_model"];
        [self.img_driver_profile downloadFromURL:[walker valueForKey:@"picture"] withPlaceholder:nil];
        self.lblCarType.text=[walker valueForKey:@"car_type"];
        self.lblCarNumber.text=[walker valueForKey:@"car_number"];
    }
}

#pragma mark-
#pragma mark- Show Route With Google

-(void)DrawPath:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t
{
    //if(routes)
    //{
    [mapView_ clear];
    //}
    
    GMSMarker *markerOwner = [[GMSMarker alloc] init];
    markerOwner.position = f;
    markerOwner.icon = [UIImage imageNamed:@"pin_starting_point"];
    markerOwner.map = mapView_;
    
    GMSMarker *markerDriver = [[GMSMarker alloc] init];
    markerDriver.position = t ;
    
    markerDriver.icon = [UIImage imageNamed:@"pin_destination"];
    markerDriver.map = mapView_;
    
    routes = [self calculateRoute:f to:t];
    
    [self centerMap:routes];
    //  NSInteger numberOfSteps = routes.count;
    
    /*   NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
     NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
     
     
     NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
     //chk
     
     NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
     
     NSError* error = nil;
     NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
     
     NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
     if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] ||  [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
     {
     }
     else
     {
     GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
     GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
     singleLine.strokeWidth = 4.0f;
     singleLine.strokeColor = [UIColor colorWithRed:27.0/255.0 green:151.0/255.0 blue:200.0/255.0 alpha:1.0];
     singleLine.map = mapView_;
     
     // routes = json[@"routes"];
     
     
     }
     //    [self centerMap:[self decodePolyLine:json[@"routes"]]];
     
     [self centerMapFirst:f two:t third:t];*/
}

-(NSArray*)calculateRoute:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    //chk
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] ||  [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
    }
    else
    {
        GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
        GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 4.0f;
        singleLine.strokeColor = [UIColor colorWithRed:27.0/255.0 green:151.0/255.0 blue:200.0/255.0 alpha:1.0];
        singleLine.map = mapView_;
        
        routes = json[@"routes"];
        
        
    }
    NSString *points = @"";
    if ([routes count] > 0)
    {
        points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
        
    }
    
    return [self decodePolyLine:[points mutableCopy]];
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init] ;
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude=[[NSNumber alloc] initWithFloat:lat *1e-5];
        NSNumber *longitude=[[NSNumber alloc] initWithFloat:lng *1e-5];
        
        printf("[%f,", [latitude doubleValue]);
        printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]] ;
        [array addObject:loc];
    }
    
    return array;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForApplicationType.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CarTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cartype" forIndexPath:indexPath];
    
    NSDictionary *dictType=[arrForApplicationType objectAtIndex:indexPath.row];
    if (strForTypeid==nil || [strForTypeid isEqualToString:@"0"])
    {
        if ([[dictType valueForKey:@"is_default"]intValue]==1)
        {
            for(CarTypeDataModal *obj in arrForApplicationType)
            {
                obj.isSelected = NO;
            }
            CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
            obj.isSelected = YES;
            [self getETA:[arrDriver objectAtIndex:0]];
            NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
            strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"min_fare"]];
            strPassCap=[NSString stringWithFormat:@"%@",[dict valueForKey:@"max_size"]];
            self.lblETA.text=strETA;
            self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
            self.lblRate_BasePrice.text=[NSString stringWithFormat:@"$ %@",strMinFare];
            self.lblCarType.text=obj.name;
            self.lblCarNumber.text=obj.name;
            self.lblSize.text=[NSString stringWithFormat:@"%@ PERSONS",strPassCap];
            strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            [pref setObject:strMinFare forKey:PREF_FARE_AMOUNT];
            [pref synchronize];
        }
    }
    
    [cell setCellData:[arrForApplicationType objectAtIndex:indexPath.row]];
    
    //  cell.imgType.layer.masksToBounds = YES;
    //   cell.imgType.layer.opaque = NO;
    //    cell.imgType.layer.cornerRadius=18;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for(CarTypeDataModal *obj in arrForApplicationType) {
        obj.isSelected = NO;
    }
    CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
    obj.isSelected = YES;
    NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
    strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"min_fare"]];
    strPassCap=[NSString stringWithFormat:@"%@",[dict valueForKey:@"max_size"]];
    if ([strForTypeid intValue] !=[obj.id_ intValue])
    {
        // [self selectServiceBtnPressed:nil];
        self.lblETA.text=strETA;
        self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
        self.lblRate_BasePrice.text=[NSString stringWithFormat:@"$ %@",strMinFare];
        self.lblCarType.text=obj.name;
        self.lblCarNumber.text=obj.name;
        self.lblSize.text=[NSString stringWithFormat:@"%@ PERSONS",strPassCap];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        [pref setObject:strMinFare forKey:PREF_FARE_AMOUNT];
        [pref synchronize];
    }
    strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
    
    [self showProvider];
    [self.collectionView reloadData];
}



#pragma mark
#pragma mark - UITextfield Delegate


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *strFullText=[NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if(self.txtAddress==textField)
    {
        if(arrForAddress.count==1)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x,100+134, self.tableView.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 100+78, self.tableView.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 100+34, self.tableView.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableView.hidden=YES;
        
        [self.tableView reloadData];
        
        
    }
    else if (self.txtDestination==textField)
    {
        if(arrForAddress.count==1)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x,230+134, self.tableView.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 230+78, self.tableView.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 230+34, self.tableView.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableView.hidden=YES;
        
        [self.tableView reloadData];
        
        
    }
    
    
    
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        self.txtAddress.text=@"";
        [self.viewForRequest setHidden:YES];
        [self.blurViewForRequest setHidden:YES];
        
    }
    if (textField==self.txtPreferral)
    {
        self.viewForReferralError.hidden=YES;
    }
    if(textField==self.txtDestination)
    {
        //isDest=1;
        // self.txtDestination.text=@"";
        self.viewForRequest.hidden=YES;
        self.blurViewForRequest.hidden = YES;
    }
    
    if(self.txtAddress==textField)
    {
        self.tableForCity.frame=CGRectMake(self.tableForCity.frame.origin.x
                                           , self.viewForAddress.frame.origin.y+35.0f, self.tableForCity.frame.size.width, self.tableForCity.frame.size.height);
    }
    else if(self.txtDestination==textField)
    {
        self.tableForCity.frame=CGRectMake(self.tableForCity.frame.origin.x, self.viewForAddress.frame.origin.y+68.0f, self.tableForCity.frame.size.width, self.tableForCity.frame.size.height);
    }
    
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==self.txtAddress && [self.txtAddress.text length] > 1)
    {
        client_marker.map=nil;
        isDest = 1;
        
        [self.txtDestination setText:@""];
        [self.viewForRequest setHidden:YES];
        [self.blurViewForRequest setHidden: YES];
        
        [self getLocationFromString:self.txtAddress.text Type:@"source"];
    }
    else if (textField==self.txtDestination && [self.txtAddress.text length] > 1)
    {
        destination_marker.map=nil;
        isDest = 2;

        NSString *reqID = [USERDEFAULT objectForKey:PREF_REQ_ID];
        if (!reqID)
        {
            
            [self getLocationFromString:self.txtDestination.text Type:@"destination"];
            
        }
    }
    if (textField == self.txtNotes) {
        
        [self.viewForAdditionalPassenger setFrame:CGRectMake(0, self.positionForPassengerView.origin.y, self.viewForAdditionalPassenger.frame.size.width, self.viewForAdditionalPassenger.frame.size.height)];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (isDest == 3) {
        if (textField == self.txtAddress) {
            
            isDest = 1;
            [self.txtAddress setText:@""];

        }
        else{
            if (textField == self.txtDestination) {
                isDest = 2;
                [self.txtDestination setText:@""];
            }
            

        }
        [mapView_ clear];
        [self.viewForMarker setHidden:NO];

    }
    if (textField == self.txtNotes) {
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    // self.tableForCountry.frame=tempCountryRect;
    //  self.tblFilterArtist.frame=tempArtistRect;
    
    if(textField==self.txtAddress)
    {
        self.tableView.hidden=YES;
        [self.txtAddress resignFirstResponder];
    }
    else if(textField==self.txtDestination)
    {
        self.tableView.hidden=YES;
        [self.txtDestination resignFirstResponder];
    }
    else if (textField == self.txtNotes){
        [self.txtNotes resignFirstResponder];
        
        [self.viewForAdditionalPassenger setFrame:CGRectMake(0, self.positionForPassengerView.origin.y, self.viewForAdditionalPassenger.frame.size.width, self.viewForAdditionalPassenger.frame.size.height)];
    }
    return YES;
}


-(void)getLocationFromString:(NSString *)str
{
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             
             if ([arrAddress count] > 0)
                 
             {
                 
                 self.txtAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 
                 NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                 
                 strForLatitude=[dictLocation valueForKey:@"lat"];
                 strForLongitude=[dictLocation valueForKey:@"lng"];
                 [self getETA:[arrDriver objectAtIndex:0]];
                 CLLocationCoordinate2D coor;
                 coor.latitude=[strForLatitude doubleValue];
                 coor.longitude=[strForLongitude doubleValue];
                 
                 
                 GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
                 [mapView_ animateWithCameraUpdate:updatedCamera];
                 // [self getProviders];
                 
                 
             }
             
         }
         
     }];
}


-(NSInteger)tableView:(UITableView *)tableView
{
    return arrForCards.count;
}

- (IBAction)paymentNotAddedNotice:(id)sender {
    
    UIAlertView *alertPayment = [[UIAlertView alloc]initWithTitle:@"Add Payment"
                                                          message:@"Oops, you have to add payment to make a request. Would you like to add a card now?"
                                                         delegate:self
                                                cancelButtonTitle:@"No"
                                                otherButtonTitles:@"Yes",nil];
    alertPayment.tag=40;
    [alertPayment show];
}



-(void)getLocationFromString:(NSString *)str Type:(NSString *)type
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    //    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString * strForUserId=[pref objectForKey:PREF_USER_ID];
    NSString * strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_CARDS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
    AFNHelper *afnn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afnn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
     {
         
         NSLog(@"History Data= %@",response);
         if (response)
         {
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [APPDELEGATE hideLoadingView];
                 [arrForCards removeAllObjects];
                 [arrForCards addObjectsFromArray:[response valueForKey:@"payments"]];
                 /* if(arrForCards.count>0)
                  {
                  _btnRideNow.enabled=YES;
                  _btnPickMeUp.enabled=YES;
                  _paymentNotAddedNotice.enabled=NO;
                  }
                  else
                  {
                  
                  _paymentNotAddedNotice.enabled=YES;
                  _btnRideNow.enabled=NO;
                  _btnPickMeUp.enabled=NO;
                  UIAlertView *alertPayment2 = [[UIAlertView alloc]initWithTitle:@"Add Payment"
                  
                  message:@"Oops, you have to add payment to make a request, would you like to add a card now? "
                  
                  delegate:self
                  
                  cancelButtonTitle:@"No"
                  
                  otherButtonTitles:@"Yes",nil];
                  
                  alertPayment2.tag=60;
                  [alertPayment2 show];
                  
                  
                  
                  }*/
                 
             }
             
         }
         
     }
     ];
    
    __block CLLocationCoordinate2D source;
    __block  CLLocationCoordinate2D coor;
    
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             //__block CLLocationCoordinate2D source;
             //__block  CLLocationCoordinate2D coor;
             
             if ([arrAddress count] > 0)
                 
             {
                 if([type isEqualToString:@"source"])
                 {
                     
                     
                     if (isList) {
                     }
                     else{
                         self.txtAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     }
                     NSArray *arrPartialMatch=[[response valueForKey:@"results"]valueForKey:@"partial_match"];
                     if (arrPartialMatch.count==0 || [[arrPartialMatch objectAtIndex:0] isKindOfClass:[NSNull class]])
                     {
                         
                         
                         strForSourceAddress=self.txtAddress.text;
                         NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                         
                         strForLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                         strForLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                         source.latitude=[strForLatitude doubleValue];
                         source.longitude=[strForLongitude doubleValue];
                         
                         GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:source zoom:14];
                         [mapView_ animateWithCameraUpdate:updatedCamera];
                         
                         
                         [self getAddress:@"source"];
                     }
                     else
                     {
                         AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
                         [helper getLocationFromPlaceIdwithParamData:placeID withBlock:^(id response, NSError *error) {
                             if (response)
                             {
                                 if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                                     strForSourceAddress=self.txtAddress.text;
                                     NSDictionary *dictLocation=[[[response valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
                                     
                                     strForLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                                     strForLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                                     source.latitude=[strForLatitude doubleValue];
                                     source.longitude=[strForLongitude doubleValue];
                                     
                                     
                                     
                                     
                                     
                                     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:source zoom:14];
                                     [mapView_ animateWithCameraUpdate:updatedCamera];
                                     
                                     
                                     [self getAddress:@"source"];
                                     
                                 }
                             }
                         }];
                     }
                 }
                 else if([type isEqualToString:@"destination"])
                 {
                     if (isList) {
                         isList = NO;
                     }
                     else{
                         self.txtDestination.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     }
                     
                     NSArray *arrPartialMatch=[[response valueForKey:@"results"]valueForKey:@"partial_match"];
                     if (arrPartialMatch.count==0 || [[arrPartialMatch objectAtIndex:0] isKindOfClass:[NSNull class]])
                     {
                         
                         strForDestinationAddress=self.txtDestination.text;
                         NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                         strDestLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                         strDestLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                         
                         source.latitude=[strForLatitude doubleValue];
                         source.longitude=[strForLongitude doubleValue];
                         coor.latitude=[strDestLatitude doubleValue];
                         coor.longitude=[strDestLongitude doubleValue];
                         [self getDistanceAndCost];
                         
                     }
                     else{
                         AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
                         [helper getLocationFromPlaceIdwithParamData:placeID withBlock:^(id response, NSError *error) {
                             if (response)
                             {
                                 if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                                     strForDestinationAddress=self.txtDestination.text;
                                     NSDictionary *dictLocation=[[[response valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
                                     strDestLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                                     strDestLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                                     
                                     source.latitude=[strForLatitude doubleValue];
                                     source.longitude=[strForLongitude doubleValue];
                                     coor.latitude=[strDestLatitude doubleValue];
                                     coor.longitude=[strDestLongitude doubleValue];
                                     
                                     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:source zoom:14];
                                     [mapView_ animateWithCameraUpdate:updatedCamera];
                                     [self getDistanceAndCost];
                                     
                                     
                                 }
                             }
                         }];
                         
                     }
                     
                     
                     isList = YES;
                     
                     AppDelegate *obj = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                     NSLog(@"GENDER %@", obj.user_gender );
                     
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
                     
                     [UIView animateWithDuration:0.5 animations:^{
                         if([[dictInfo valueForKey:@"gender"]isEqualToString:@"Female"])
                         {
                             
                             // self.viewForRequest.hidden=NO;
                             self.female_only.hidden=NO;
                             self.femaleOnlyLabel.hidden=NO;
                             self.female_only_switch.hidden=NO;
                             self.femaleSwitch.hidden=NO;
                             
                         }
                         else
                         {
                             self.female_only.hidden=YES;
                             self.femaleOnlyLabel.hidden=YES;
                             self.female_only_switch.hidden=YES;
                             self.femaleSwitch.hidden = YES;
                             // self.viewForRequest.hidden=NO;
                             
                         }
                         
                         
                     } completion:^(BOOL finished)
                      {
                      }];
                     
                     
                     
                 }
                 
             }
             else
             {
                 AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
                 [helper getLocationFromPlaceIdwithParamData:placeID withBlock:^(id response, NSError *error) {
                     if (response)
                     {
                         if([type isEqualToString:@"source"])
                         {
                             if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                                 strForSourceAddress=self.txtAddress.text;
                                 NSDictionary *dictLocation=[[[response valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
                                 
                                 strForLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                                 strForLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                                 source.latitude=[strForLatitude doubleValue];
                                 source.longitude=[strForLongitude doubleValue];
                                 
                                 
                                 
                                 
                                 
                                 GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:source zoom:14];
                                 [mapView_ animateWithCameraUpdate:updatedCamera];
                                 
                                 
                                 //[self getAddress:@"source"];
                                 
                             }
                         }
                         else
                         {
                             strForDestinationAddress=self.txtDestination.text;
                             NSDictionary *dictLocation=[[[response valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
                             strDestLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                             strDestLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                             
                             source.latitude=[strForLatitude doubleValue];
                             source.longitude=[strForLongitude doubleValue];
                             coor.latitude=[strDestLatitude doubleValue];
                             coor.longitude=[strDestLongitude doubleValue];
                             
                             GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:source zoom:14];
                             [mapView_ animateWithCameraUpdate:updatedCamera];
                             [self getDistanceAndCost];
                             
                         }
                         
                     }
                 }];
             }
         }
         
         
         
     }];
}

-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
    
}


-(void)centerMapFirst:(CLLocationCoordinate2D)pos1 two:(CLLocationCoordinate2D)pos2 third:(CLLocationCoordinate2D)pos3
{
    GMSCoordinateBounds* bounds =
    [[GMSCoordinateBounds alloc]initWithCoordinate:pos1 coordinate:pos2];
    bounds=[bounds includingCoordinate:pos3];
    CLLocationCoordinate2D location1 = bounds.southWest;
    CLLocationCoordinate2D location2 = bounds.northEast;
    
    float mapViewWidth = mapView_.frame.size.width;
    float mapViewHeight = mapView_.frame.size.height;
    
    MKMapPoint point1 = MKMapPointForCoordinate(location1);
    MKMapPoint point2 = MKMapPointForCoordinate(location2);
    
    MKMapPoint centrePoint = MKMapPointMake(
                                            (point1.x + point2.x) / 2,
                                            (point1.y + point2.y) / 2);
    CLLocationCoordinate2D centreLocation = MKCoordinateForMapPoint(centrePoint);
    
    double mapScaleWidth = mapViewWidth / fabs(point2.x - point1.x);
    double mapScaleHeight = mapViewHeight / fabs(point2.y - point1.y);
    double mapScale = MIN(mapScaleWidth, mapScaleHeight);
    
    double zoomLevel = 19.5 + log2(mapScale);
    
    // changeCameraPosition = YES;
    
    
    // if (changeCameraPosition) {
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:centreLocation zoom: zoomLevel];
    [mapView_ animateWithCameraUpdate:updatedCamera];
    // }
    
}

#pragma mark -
#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex]isEqualToString:@"Pay via Card"])
    {
        is_paymetCard = YES;
        [self.paymodeModeBtn setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
        
    }
    if ([[actionSheet buttonTitleAtIndex:buttonIndex]isEqualToString:@"Pay via Cash"])
    {
        is_paymetCard = NO;
        [self.paymodeModeBtn setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
        
    }
    
}

#pragma mark -
#pragma mark - Referral btn Action

- (IBAction)btnSkipReferral:(id)sender
{
    Referral=@"1";
    [self createService];
}

- (IBAction)btnAddReferral:(id)sender
{
    Referral=@"0";
    [self createService];
}


-(void)getDistanceAndCost
{
    if ([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please wait..."];
        NSMutableDictionary *dictparam = [[NSMutableDictionary alloc]init];
        [dictparam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictparam setObject:strForLatitude forKey:PARAM_SOURCE_LATITUDE];
        [dictparam setObject:strForLongitude forKey:PARAM_SOURCE_LONGITUDE];
        [dictparam setObject:strDestLatitude forKey:PARAM_DESTIANTION_LATITUDE];
        [dictparam setObject:strDestLongitude forKey:PARAM_DESTIANTION_LONGITUDE];
        
        
        CLLocationCoordinate2D source = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
        CLLocationCoordinate2D destination = CLLocationCoordinate2DMake([strDestLatitude doubleValue], [strDestLongitude doubleValue]);
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_GET_USER_INFO withParamData:dictparam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                if ([[response valueForKey:@"success"] boolValue])
                {
                    NSDictionary *details = [response valueForKey:@"details"];
                    
                    if ([[details valueForKey:@"distance"] integerValue] > 0) {
                        [self.lblDistance setText:[NSString stringWithFormat:@"%.2f Km",[[details valueForKey:@"distance"] doubleValue]]];
                        [self.lblPrice setText:[NSString stringWithFormat:@"$%@",[details valueForKey:@"total"]]];
                        strDistance = [NSString stringWithFormat:@"%@",[details valueForKey:@"distance"]];
                        [self.viewForRequest setHidden:NO];
                        [self.blurViewForRequest setHidden:NO];
                        [self DrawPath:source to:destination];
                        isDest = 3;
                        [self.viewForMarker setHidden:YES];
                        
                    }
                    
                    else{
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"NOT_VALID_LOCATION", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                    
                    
                }
                else{
                    if([response valueForKey:@"error"]){
                        
                        if ([[response valueForKey:@"error"] isEqualToString:@"User Not Found"] || [[response valueForKey:@"error"] isEqualToString:@"Not a valid token"]) {
                            [USERDEFAULT setBool:NO forKey:PREF_IS_LOGIN];
                            [USERDEFAULT removeObjectForKey:PREF_LOGIN_OBJECT];
                            [USERDEFAULT synchronize];
                            [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login expired" message:@"Please relogin." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alert show];                        }
                        else{
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alert show];
                        }
                        
                    }

                }
            }
            else
            {
                
                    [self.viewForRequest setHidden:YES];
                    [self.blurViewForRequest setHidden:YES];
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                
                
                
                
            }
            
        }];
    }
}

-(void)createService
{
    self.viewForReferralError.hidden=YES;
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:self.txtPreferral.text forKey:PARAM_REFERRAL_CODE];
        [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:Referral forKey:PARAM_REFERRAL_SKIP];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                         [pref synchronize];
                         self.viewForPreferral.hidden=YES;
                         self.btnMyLocation.hidden=NO;
                         self.btnETA.hidden=NO;
                         self.navigationController.navigationBarHidden=NO;
                         self.txtPreferral.text=@"";
                         if([Referral isEqualToString:@"0"])
                         {
                             [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                         }
                         // [self setTimerToCheckDriverStatus];
                         self.navigationController.navigationBarHidden=NO;
                         [self getAllApplicationType];
                         [super setNavBarTitle:TITLE_PICKUP];
                         [self customSetup];
                         [self checkForAppStatus];
                         [self getPagesData];
                         [self getProviders];
                         [self.paymentView setHidden:YES];
                         self.viewETA.hidden=YES;
                         [self cashBtnPressed:nil];
                     }
                 }
                 else
                 {
                     self.txtPreferral.text=@"";
                     self.viewForReferralError.hidden=NO;
                     self.lblReferralMsg.text=[response valueForKey:@"error"];
                     self.lblReferralMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

- (IBAction)onClickDateSelected:(id)sender {
    
    NSDate *date =  self.RLDatePicker.date;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"SGT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2013-07-15:10:00:00
    strFutureDate = [formatter stringFromDate:date];
    
    NSString *strDate = [[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"];
    NSString *strTime  = [[UtilityClass sharedObject] DateToString:date withFormate:@"HH:mm a"];
    
    [self.lblDepartureDate setText:[NSString stringWithFormat:@"%@",strDate]];
    [self.lblDepartureTime setText:strTime];
    
    
    [self.lblAdvanceFrom setText:self.txtAddress.text];
    [self.lblAdvanceTo setText:self.txtDestination.text];
    [self.lblAdvanceDistance setText:self.lblDistance.text];
    [self.lblAdvanceCost setText:self.lblPrice.text];
    
    [self.viewForRequest setHidden:YES];
    [self.blurViewForRequest setHidden:YES];
    [self.viewForDatePick setHidden:YES];
//    [self.viewForAddress setHidden:YES];
    [self.viewForAdditionalPassenger setHidden:NO];
    
}

- (IBAction)onClickGoHideDateView:(id)sender {
    
    [self.viewForDatePick setHidden:YES];
    [self.viewForRequest setHidden:NO];
    [self.blurViewForRequest setHidden:NO];
    [self.viewForConfirmRequest setHidden:YES];
    [self.viewForConfirmRequestTwo setHidden:YES];
    [self.editBtn setUserInteractionEnabled:NO];
    [self.confirmBtn setUserInteractionEnabled:NO];
}

- (IBAction)onClickCancelAdvanceRide:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to cancel advance ride request?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [alert setTag:9];
    [alert show];
    
}

- (IBAction)onClickAdvanceEdit:(id)sender {
    
    [self.viewForRequest setHidden:YES];
    [self.blurViewForRequest setHidden:YES];
    [self reloadDatePicker];
    [self.viewForDatePick setHidden:NO];
    [self.viewForConfirmRequest setHidden:YES];
    [self.viewForConfirmRequestTwo setHidden:YES];
    [self.editBtn setUserInteractionEnabled:NO];
    [self.confirmBtn setUserInteractionEnabled:NO];
//    [self.viewForAddress setHidden:NO];
}

- (IBAction)onClickAdvanceConfirm:(id)sender {
    
    futureRequest = 1;
    [self createFutureRequest];
    //[self.editBtn setUserInteractionEnabled:NO];
    //[self.confirmBtn setUserInteractionEnabled:NO];
}

-(void)createFutureRequest{
    
    if([CLLocationManager locationServicesEnabled])
    {
        if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
        {
            strForTypeid=@"1";
        }
        if(![strForTypeid isEqualToString:@"0"])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else if(((strDestLongitude==nil)&&(strForDriverLatitude==nil))
                    ||(([strDestLongitude doubleValue]==0.00)&&([strForDriverLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else
            {
                if([[AppDelegate sharedAppDelegate]connected])
                {
                    
                    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    
                    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
                    [dictParam setValue:strDestLatitude forKey:PARAM_DESTIANTION_LATITUDE];
                    [dictParam setValue:strDestLongitude  forKey:PARAM_DESTIANTION_LONGITUDE];
                    [dictParam setValue:self.txtAddress.text forKey:PARAM_SOURCE_ADDRESS];
                    [dictParam setValue:self.txtDestination.text forKey:PARAM_DESTIANTION_ADDRESS];
                    [dictParam setValue:@"1" forKey:FUTURE_REQUEST];
                    [dictParam setValue:strFutureDate forKey:DATE_TIME];
                    [dictParam setValue:gender forKey:PARAM_GENDER];
                    [dictParam setValue:strForUserId forKey:PARAM_ID];
                    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                    if (is_paymetCard)
                    {
                        [dictParam setValue:@"0" forKey:PARAM_PAYMENT_MODE];
                    }
                    else
                    {
                        [dictParam setValue:@"1" forKey:PARAM_PAYMENT_MODE];
                    }
                    
                    [dictParam setValue:[NSString stringWithFormat:@"%d",(int)self.stepper.value] forKey:PARAM_NO_OF_PASSENGER];
                    [dictParam setValue:self.txtNotes.text forKey:PARAM_NOTE];
                    
                    [self.txtNotes setText:@""];
                    [APPDELEGATE showLoadingWithTitle:@"Creating Advance Request"];
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_FUTURE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [APPDELEGATE hideLoadingView];
                         
                         if (response)
                         {
                             if([[response valueForKey:@"success"]boolValue])
                             {
                                 NSLog(@"Advance Request Response......%@",response);
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     
                                     [self.viewForRequest setHidden:NO];
                                     [self.blurViewForRequest setHidden:NO];
                                     [self.viewForDatePick setHidden:YES];
                                     [self.viewForConfirmRequest setHidden:YES];
                                     [self.viewForConfirmRequestTwo setHidden:YES];
                                     [self.editBtn setUserInteractionEnabled:NO];
                                     [self.confirmBtn setUserInteractionEnabled:NO];
                                     [self performSegueWithIdentifier:SEGUE_TO_RIDE_NEXT sender:self];
                                     
                                 }
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                                 
                             }
                         }
                         else
                         {
                             /* if (futureRequest < 4) {
                              [self createFutureRequest];
                              futureRequest++;
                              }
                              else{
                              */
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Can not process request, try again later." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             [alert show];
                             
                             // }
                         }
                     }];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                }
            }
            
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
    
}
- (IBAction)onClickPaymentMode:(id)sender {
    
    [_paymentSheet showInView:self.view];
}

- (IBAction)onClickConfirmPassengers:(id)sender {
    [self.viewForAdditionalPassenger setHidden:YES];
    
    [self.viewForConfirmRequest setHidden:NO];
    [self.viewForConfirmRequestTwo setHidden:NO];
    [self.editBtn setUserInteractionEnabled:YES];
    [self.confirmBtn setUserInteractionEnabled:YES];
    [self.txtNotes resignFirstResponder];
    
    
}

- (IBAction)addSubtractPassengers:(id)sender {
    
    [self.lblNumOfPassenger setText:[NSString stringWithFormat:@"Number of Passenger : %d",(int)_stepper.value]];
    
}

- (IBAction)goBackToDateFromAddtionalPassenger:(id)sender {
    
    [self.viewForAdditionalPassenger setHidden:YES];
    [self.viewForDatePick setHidden:NO];
    [self.viewForConfirmRequest setHidden:YES];
    [self.viewForConfirmRequestTwo setHidden:YES];
    [self.editBtn setUserInteractionEnabled:NO];
    [self.confirmBtn setUserInteractionEnabled:NO];
    [self.viewForAdditionalPassenger setFrame:CGRectMake(0, self.positionForPassengerView.origin.y, self.viewForAdditionalPassenger.frame.size.width, self.viewForAdditionalPassenger.frame.size.height)];
}



- (IBAction)onClickChatBtn:(id)sender {
    
   // if (isQuickBlox) {
        [self performSegueWithIdentifier:@"segueToDialogs" sender:self];
   // }
   
}
@end

