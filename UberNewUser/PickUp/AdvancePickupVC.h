

#import "BaseVC.h"
#import  <GoogleMaps/GoogleMaps.h>
#import "SWRevealViewController.h"
#import "Constants.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import  <MapKit/MapKit.h>
#import "RatingBar.h"

@interface AdvancePickupVC : BaseVC<GMSMapViewDelegate,UIAlertViewDelegate>
{
    GMSMapView *mapView_;
    
    NSArray *routes;
    
    QBUUser *receipent;
}

@property (strong,nonatomic) NSDictionary *dictRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForGoogleMap;
@property (weak, nonatomic) IBOutlet UIView *frontView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;
@property (weak, nonatomic) IBOutlet UIImageView *UserImgView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblPassengerCount;
@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (weak, nonatomic) IBOutlet UILabel *verticalLine3;
@property (weak, nonatomic) IBOutlet UILabel *lblNote_Label;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UIButton *btnOpenInMaps;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIView *roundedRectView;
@property (weak, nonatomic) IBOutlet UIImageView *triangleImgView;
@property (weak, nonatomic) IBOutlet UILabel *lblBtnsBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;

@property (weak, nonatomic) IBOutlet UIView *viewForPlace;
@property (weak, nonatomic) IBOutlet UIView *viewForDoc;
@property (weak, nonatomic) IBOutlet UIView *viewForPerson;

@property (weak, nonatomic) IBOutlet UIButton *btnPlaceIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnDocIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonIcon;
//Chat
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;


@property (strong,nonatomic) NSString *isOffer;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickAccept:(id)sender;
- (IBAction)onClickReject:(id)sender;
- (IBAction)toMaps:(id)sender;
- (IBAction)onClickChat:(id)sender;
- (IBAction)onClickPlaceIcon:(id)sender;
- (IBAction)onClickDocIcon:(id)sender;
- (IBAction)onClickPersonIcon:(id)sender;



- (void)slideUpWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
- (void)slideDownWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer;

@end
