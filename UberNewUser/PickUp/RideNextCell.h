//
//  RideNextCell.h
//  SwiftBack
//
//  Created by My Mac on 10/15/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "RatingBar.h"


@interface RideNextCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblFromStation;
@property (weak, nonatomic) IBOutlet UILabel *lblToStation;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundCell;

@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;


@end
