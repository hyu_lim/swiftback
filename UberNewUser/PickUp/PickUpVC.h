//
//  PickUpVC.h
//  UberNewUser
//
//  Developed by Elluminati on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>


@interface PickUpVC : BaseVC<MKMapViewDelegate,CLLocationManagerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,GMSMapViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    CLLocationManager *locationManager;
    NSDictionary* aPlacemark;
    
    NSMutableArray *placeMarkArr;
    
    NSMutableArray *arrPaymentTypes;
    
    NSString *strFutureDate;
    
    NSString *strDistance;
    
    NSArray *routes;
	NSInteger time;
    
    BOOL isList,changeCameraPosition,isQuickBlox;
    GMSMutablePath *pathpoliline;
    
    NSString *placeID;
    NSInteger DistanceETACount;
    
    QBUUser *currentUser;
    
}

/////// Outlets

@property (weak, nonatomic) IBOutlet UITableView *tableForCity;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewGoogleMap;
@property (weak, nonatomic) IBOutlet UILabel *lblNavigation;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property UIActionSheet *paymentSheet;

@property (weak, nonatomic) IBOutlet UIView *viewForCancel;

@property (weak, nonatomic) IBOutlet UIButton *female_only_switch;

@property (strong,nonatomic) NSTimer *timerForCheckReqStatus;
@property (strong,nonatomic) NSTimer *timerForRequestInProgress;

@property (nonatomic) IBOutlet UIButton* revealButtonItem;
@property (nonatomic,weak) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UIView *viewForMarker;
@property (weak, nonatomic) IBOutlet UITextField *txtPreferral;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnETA;
@property (weak, nonatomic) IBOutlet UIView *viewForPreferral;
@property (weak, nonatomic) IBOutlet UIView *viewForFareAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblFareAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForReferralError;
@property (weak, nonatomic) IBOutlet UILabel *lblReferralMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

/////// Actions
@property (weak, nonatomic) IBOutlet UIButton *btnSelService;
@property (weak, nonatomic) IBOutlet UIButton *btnPickMeUp;

- (IBAction)pickMeUpBtnPressed:(id)sender;
- (IBAction)cancelReqBtnPressed:(id)sender;

- (IBAction)myLocationPressed:(id)sender;
- (IBAction)selectServiceBtnPressed:(id)sender;

-(void)goToSetting:(NSString *)str;
-(void)goToRide;
-(void)rideNext;

@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet UIButton *btnCash;
@property (weak, nonatomic) IBOutlet UIButton *btnCard;

- (IBAction)ETABtnPressed:(id)sender;
- (IBAction)cashBtnPressed:(id)sender;
- (IBAction)cardBtnPressed:(id)sender;
- (IBAction)requestBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)btnSkipReferral:(id)sender;
- (IBAction)btnAddReferral:(id)sender;

//ETA View

@property (weak, nonatomic) IBOutlet UIView *viewETA;
@property (weak, nonatomic) IBOutlet UILabel *lblETA;
@property (weak, nonatomic) IBOutlet UILabel *lblSize;
@property (weak, nonatomic) IBOutlet UILabel *lblFare;

@property (weak, nonatomic) IBOutlet UIButton *btnRatecard;
@property (weak, nonatomic) IBOutlet UIView *viewForRateCard;

- (IBAction)eastimateFareBtnPressed:(id)sender;
- (IBAction)closeETABtnPressed:(id)sender;
- (IBAction)RateCardBtnPressed:(id)sender;

// Payment Method

////for Localization

@property (weak, nonatomic) IBOutlet UIButton *btnFare;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lMinFare;
@property (weak, nonatomic) IBOutlet UILabel *lETA;
@property (weak, nonatomic) IBOutlet UILabel *lMaxSize;
@property (weak, nonatomic) IBOutlet UILabel *lSelectPayment;
@property (weak, nonatomic) IBOutlet UIButton *btnPayCancel;

@property (weak, nonatomic) IBOutlet UIButton *btnPayRequest;

@property (weak, nonatomic) IBOutlet UILabel *lRefralMsg;
@property (weak, nonatomic) IBOutlet UIButton *bReferralSubmit;

@property (weak, nonatomic) IBOutlet UIButton *bReferralSkip;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundCancel;

//// view for rate card
@property (weak, nonatomic) IBOutlet UILabel *lRate_basePrice;
@property (weak, nonatomic) IBOutlet UILabel *lRate_distancecost;
@property (weak, nonatomic) IBOutlet UILabel *lRate_TimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblRate_BasePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRate_DistancePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRate_TimePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRateCradNote;
@property (weak, nonatomic) IBOutlet UILabel *lblCarType;
@property (weak, nonatomic) IBOutlet UILabel *lblCarNumber;

///// for driver detail
@property (weak, nonatomic) IBOutlet UIView *viewForDriver;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driverName;
@property (weak, nonatomic) IBOutlet UIImageView *img_driver_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driverRate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driver_Carname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driver_CarNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtDestination;
@property (weak, nonatomic) IBOutlet UIView *viewForAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForRequest;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIView *viewForAcceptRequest;
@property (weak, nonatomic) IBOutlet UILabel *female_only;
@property (weak, nonatomic) IBOutlet UISwitch *femaleSwitch;

- (IBAction)DestinationSearching:(id)sender;
- (IBAction)onClickSwitch:(id)sender;
- (IBAction)onClickRideLater:(id)sender;

//Ride Later
@property (weak, nonatomic) IBOutlet UIView *viewForDatePick;
@property (weak, nonatomic) IBOutlet UIDatePicker *RLDatePicker;

- (IBAction)onClickDateSelected:(id)sender;
- (IBAction)onClickGoHideDateView:(id)sender;


//Advanced Ride
@property (weak, nonatomic) IBOutlet UIView *viewForConfirmRequest;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceTo;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDate;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *viewForConfirmRequestTwo;

- (IBAction)onClickCancelAdvanceRide:(id)sender;
- (IBAction)onClickAdvanceEdit:(id)sender;
- (IBAction)onClickAdvanceConfirm:(id)sender;


//background Rect
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;

//main view
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *advanceBackground;
@property (weak, nonatomic) IBOutlet UILabel *femaleOnlyLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnRideNow;
@property (weak, nonatomic) IBOutlet UIButton *paymentNotAddedNotice;
@property (weak, nonatomic) IBOutlet UIButton *paymodeModeBtn;

- (IBAction)onClickPaymentMode:(id)sender;


//Notes & Passenger count
@property (weak, nonatomic) IBOutlet UIView *viewForAdditionalPassenger;
@property (weak, nonatomic) IBOutlet UILabel *lblNumOfPassenger;
@property (weak, nonatomic) IBOutlet UITextField *txtNotes;
@property (weak, nonatomic) IBOutlet UIButton *additionalPassengerBtn;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property CGRect positionForPassengerView;

- (IBAction)onClickConfirmPassengers:(id)sender;
- (IBAction)addSubtractPassengers:(id)sender;
- (IBAction)goBackToDateFromAddtionalPassenger:(id)sender;

//Chat
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;

- (IBAction)onClickChatBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *blurViewForRequest;

@end
