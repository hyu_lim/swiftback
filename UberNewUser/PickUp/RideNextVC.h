//
//  RideNextVC.h
//  SwiftBack
//
//  Created by My Mac on 10/15/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "SWRevealViewController.h"
#import <NYSegmentedControl/NYSegmentedControl.h>
#import "RatingBar.h"

@interface RideNextVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,SWTableViewCellDelegate>
{
    
    __weak IBOutlet UITableView *tblRideNext;
    __weak IBOutlet UIView *viewForNavigation;
    __weak IBOutlet UIImageView *no_Items;
    
    NSMutableArray *arrRequests;
    __weak IBOutlet UIButton *menuBtn;
    __weak IBOutlet UIButton *btnRequest;
    __weak IBOutlet UISegmentedControl *requestSegment;
}

@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedToggle;
@property (strong, nonatomic) IBOutlet UIView *mainView;

//Green Overlay View
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIImageView *dottedLineImgView;

@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImgView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImgView;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleNo;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleColor;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleName;

@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;

- (IBAction)onClickNewRequest:(id)sender;
- (IBAction)onClickSegment:(id)sender;

@end
