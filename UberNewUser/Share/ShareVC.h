//
//  ShareVC.h
//  TaxiNow
//
//  Created by My Mac on 6/23/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "BaseVC.h"
#import "Social/Social.h"
#import <MessageUI/MessageUI.h>



@interface ShareVC : BaseVC<MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}



@property (weak, nonatomic) IBOutlet UILabel *lblNavigation;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnWhatsapp;
@property (weak, nonatomic) IBOutlet UIButton *btnMail;
@property (weak, nonatomic) IBOutlet UIView *blurView;


- (IBAction)onClickFacebook:(id)sender;
- (IBAction)onClickTwitter:(id)sender;
- (IBAction)onClickWhatsapp:(id)sender;
- (IBAction)onClickMsg:(id)sender;

@end
