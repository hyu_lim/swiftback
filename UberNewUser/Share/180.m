//
//  ShareVC.m
//  TaxiNow
//
//  Created by My Mac on 6/23/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "ShareVC.h"
#import "Social/Social.h"
#import "ContactUsVC.h"
#import "SWRevealViewController.h"


@interface ShareVC ()

@end

@implementation ShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //rectangle background Rect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //rectangle background view
    _backgroundView.layer.cornerRadius = 10;
    _backgroundView.layer.borderWidth = 1;
    _backgroundView.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundView.layer.shadowRadius = 5.0;
    _backgroundView.layer.shadowOpacity = 0.4;
    
    // btnFacebook
    _btnFacebook.layer.cornerRadius = 5;
    _btnFacebook.layer.borderWidth = 0.1;
    _btnFacebook.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnWhatsapp
    _btnWhatsapp.layer.cornerRadius = 5;
    _btnWhatsapp.layer.borderWidth = 0.1;
    _btnWhatsapp.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnMail
    _btnMail.layer.cornerRadius = 5;
    _btnMail.layer.borderWidth = 0.1;
    _btnMail.layer.borderColor = [UIColor whiteColor].CGColor;
    
//    //View Controller Clipping
//    _mainView.layer.cornerRadius = 5;
//    _mainView.clipsToBounds = YES;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    self.lblTitle.text=@"Share";
    [self customSetup];
}

- (IBAction)onClickFacebook:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:@"I'm on SwiftBack. Ride sharing app for an affordable and greener ride. #swiftback #green "];
        [controller addURL:[NSURL URLWithString:@"http://www.swiftback.com"]];
        [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image-01.jpg"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
}

- (IBAction)onClickTwitter:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"I'm on SwiftBack. Ride sharing app for an affordable and greener ride. #swiftback #green - http://www.swiftback.com"];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

- (IBAction)onClickWhatsapp:(id)sender {
    
    NSString *message = @"Check out this new app. SwiftBack ride sharing app for an affordable and greener ride. http://www.swiftback.com";
    NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@",message];
    NSURL *whatsappURL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if([[UIApplication sharedApplication] canOpenURL:whatsappURL]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
    } else {
       
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"WhatsApp not installed." message:@"Your device hasn't installed WhatsApp." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)onClickMsg:(id)sender {
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"Check this out - SwiftBack Ride sharing app"];
    [mailComposer setMessageBody:@"Check out this new app. SwiftBack ride sharing app for an affordable and greener ride. http://www.swiftback.com" isHTML:YES];
    //[mailComposer setMessageBody:@"Testing message for the test mail" isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:nil];
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.lblNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        


    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
