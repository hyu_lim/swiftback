//
//  ShareVC.h
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactUsVC : BaseVC<MFMailComposeViewControllerDelegate,UIWebViewDelegate>
{
     MFMailComposeViewController *mailComposer;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic,strong) NSMutableArray *arrInformation;
@property (nonatomic,strong) NSDictionary *dictContent;
@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)btnCallClick:(id)sender;
- (IBAction)btnEmailClick:(id)sender;
- (IBAction)btnFeedBackClick:(id)sender;

@end
