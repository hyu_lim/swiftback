//
//  ShareVC.m
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ContactUsVC.h"
#import "SWRevealViewController.h"

@interface ContactUsVC ()
{
    NSString *strForHtml;
}

@end

@implementation ContactUsVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
//    [super viewDidLoad];
//     [super setBackBarItem];
//    [self.btnNavigation setTitle:[NSString stringWithFormat:@"  %@",[self.dictContent valueForKey:@"title"]] forState:UIControlStateNormal];
//    self.btnNavigation.titleLabel.font=[UberStyleGuide fontRegular];
    self.navigationController.navigationBarHidden=YES;
    
    [self.webView setDelegate:self];
    
    NSString *urlAddress =[self.dictContent valueForKey:@"content"];
    [self.webView loadHTMLString:urlAddress baseURL:nil];
    
    //rectangle background Rect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    
    //Email button
    _btnEmail.layer.cornerRadius = 5;
    _btnEmail.layer.borderWidth = 1;
    _btnEmail.layer.borderColor = [UIColor whiteColor].CGColor;
    
  
//    //View Controller Clipping
//    _mainView.layer.cornerRadius = 5;
//    _mainView.clipsToBounds = YES;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    self.lblTitle.text=@"Support";
    [self customSetup];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.lblNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    }
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnCallClick:(id)sender
{
    NSString *strProviderPhone=@"+6590999888";
    
    NSString *call=[NSString stringWithFormat:@"tel://%@",strProviderPhone];
    
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:call]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)btnEmailClick:(id)sender
{
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];

    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"SwiftBack Support Mail"];
    [mailComposer setToRecipients:[[NSArray alloc] initWithObjects:@"support@swiftback.com", nil]];
    [mailComposer setMessageBody:[NSString stringWithFormat:@"\n\n\nReference\nName:%@ %@\nUser ID:%@\nApp Version: %@\n", [dictInfo valueForKey:@"first_name"], [dictInfo valueForKey:@"last_name"],[dictInfo valueForKey:@"id"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]] isHTML:NO];
    
    [self presentViewController:mailComposer animated:YES completion:nil];
    
    
    /*
    [NSString stringWithFormat:@"Version %@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"], kRevisionNumber]

    self.userName.text=[NSString stringWithFormat:@"%@!", [dictInfo valueForKey:@"first_name"]];
     */
    
}
#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)btnFeedBackClick:(id)sender {
    
    
}
@end
