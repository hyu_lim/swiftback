//
//  HomeVC.m
//  Wag
//
//  Created by Elluminati - macbook on 20/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "HomeVC.h"
#import "LoginVC.h"
#import "RegisterVC.h"
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



@interface HomeVC ()
{
    CLLocationManager *locationManager;
    BOOL internet;
}
@property (nonatomic, strong) AVPlayer *avplayer;
@end

@implementation HomeVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad

{
    
    
    [super viewDidLoad];
    
    /*[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(move_pagination) userInfo:nil repeats:YES]; */


    
    // Create the data model
    
    
    _pageTitles = @[@" ", @"Request for rides in a tap", @"Enjoy up to 2X cheaper rates", @"Meet new people", @"Help save the environment"];
    
    _pageImages = @[@" ", @"icon_car.png", @"icon_2x.png", @"icon_people.png", @"icon_leaf.png"];
    
    _pageTitles2 = @[@"SwiftBack", @" ", @" ", @" ", @" "];
    
    _pageImages2 = @[@"arrow_logo.png", @" ", @" ", @" ", @" "];
    
    _pageTitles3 = @[@" ", @"Carpool with verified drivers around", @"VS Taxi - Cabs and private car hire", @"Enjoy a new social experience", @"By cutting down on carbon footprint"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroPageViewController"];
    self.pageViewController.dataSource = self;
    
    IntroPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 50);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    //page controller properties
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];
    
    /*_backgroundRect.layer.cornerRadius = 10;
     _backgroundRect.layer.borderWidth = 1;
     _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
     _backgroundRect.layer.shadowRadius = 5.0;
     _backgroundRect.layer.shadowOpacity = 0.4;
     */
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    
    
    //Set up player
    
    
    
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"SB_LANDING" ofType:@"mp4"]];
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x156560) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x156560) CGColor],nil];
    [self.gradintView.layer insertSublayer:gradient atIndex:0];
    
    
    [self checkStatus];
    // [self.btnSignin setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    /// [self.btnRegister setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
}



// (note - may prefer to use the tintColor of the control)

-(void)checkStatus {
    internet=[APPDELEGATE connected];
    if ([CLLocationManager locationServicesEnabled])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
        if(internet)
        {
            [self getUserLocation];
            self.lblName.text=NSLocalizedString(APPLICATION_NAME, nil);
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            if([pref boolForKey:PREF_IS_LOGIN]) {
                self.navigationController.navigationBarHidden=YES;
                [APPDELEGATE hideLoadingView];
                // [self performSegueWithIdentifier:SEGUE_TO_DIRECT_LOGIN sender:self];
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        [APPDELEGATE hideLoadingView];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> SwiftBack Driver -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    if([pref boolForKey:PREF_IS_LOGIN]) {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
        self.navigationController.navigationBarHidden=YES;
        NSDictionary  *dictLogin  = [[USERDEFAULT objectForKey:PREF_LOGIN_OBJECT] mutableCopy];
        
        
        if ([USERDEFAULT boolForKey:@"close"])
        {
            NSDate *closeDate = [USERDEFAULT objectForKey:@"closeDate"];
            NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:closeDate];
            
            if (secondsBetween <= 280)
            {
                [self performSegueWithIdentifier:SEGUE_TO_OTP sender:nil];
            }
            else
            {
                if ([[dictLogin objectForKey:@"otp_verify"] integerValue] == 1)
                {
                    [self performSegueWithIdentifier:SEGUE_TO_DIRECT_LOGIN sender:self];
                    
                }
            }
        }
        else
        {
            if ([[dictLogin objectForKey:@"otp_verify"] integerValue] == 1)
            {
                [self performSegueWithIdentifier:SEGUE_TO_DIRECT_LOGIN sender:self];
                
            }
            
        }
        
        [APPDELEGATE hideLoadingView];
    }
    if(![pref boolForKey:PREF_IS_LOGIN])
        self.navigationController.navigationBarHidden=YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    
    if(![pref boolForKey:PREF_IS_LOGIN])
        self.navigationController.navigationBarHidden=YES;
    [super viewWillDisappear:animated];
    [self.avplayer pause];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}




#pragma mark -
#pragma mark - Actions

-(IBAction)onClickSignIn:(id)sender
{
    //[self performSegueWithIdentifier:@"" sender:self];
}

-(IBAction)onClickRegister:(id)sender
{
    
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //segue.identifier
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)getUserLocation
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
}


- (IBAction)btnDriver:(id)sender {
    // A quick and dirty popup, displayed only once
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"HasSeenPopup"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"SwiftBack Drive"
                                                       message:@"Would you like to download the SwiftBack Drive app?"
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Yes",nil];
        [alert show];
    }
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // 0 = Tapped yes
    if (buttonIndex == 1)
    {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.swiftback.com/driveapp"]];
        
    }
    
}





//

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
//

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((IntroPageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((IntroPageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

/*- (void)move_pagination {
    
    
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
    
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!=10 )  {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }
}

*/


- (IntroPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    IntroPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroPageContentController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.imageFile2 = self.pageImages2[index];
    pageContentViewController.titleText2 = self.pageTitles2[index];
    pageContentViewController.titleText3 = self.pageTitles3[index];
    pageContentViewController.pageIndex = index;
    
    
    
    return pageContentViewController;
}


@end
