//
//  HomeVC.h
//  Wag
//
//  Developed by Elluminati on 20/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "introPageContentViewController.h"



@interface HomeVC : BaseVC <CLLocationManagerDelegate,UIPageViewControllerDataSource>
{
    
}


- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages2;
@property (strong, nonatomic) NSArray *pageTitles3;

@property(nonatomic,weak)IBOutlet UILabel *lblName;

-(IBAction)onClickSignIn:(id)sender;
-(IBAction)onClickRegister:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIButton *BtnDriver;

@property (weak, nonatomic) IBOutlet UIView *movieView;
@property (weak, nonatomic) IBOutlet UIView *gradintView;
@property (strong, nonatomic) IBOutlet UIView *testFrame;

@end

