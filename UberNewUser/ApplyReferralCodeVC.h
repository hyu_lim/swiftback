//
//  ApplyReferralCodeVC.h
//  UberforXOwner
//
//  Created by Deep Gami on 22/11/14.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyReferralCodeVC : UIViewController<UITextFieldDelegate,UIPageViewControllerDataSource>


- (IBAction)codeBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtPromoCode;
@property (weak, nonatomic) IBOutlet UITextField *txtCode;
- (IBAction)ContinueBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView2;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btn_Navi_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblMsg;
@property (weak, nonatomic) IBOutlet UIView *viewForReferralError;
@property (weak, nonatomic) IBOutlet UILabel *lblReferralErrorMsg;
- (IBAction)onClickBtnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *img_code;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;

@end
