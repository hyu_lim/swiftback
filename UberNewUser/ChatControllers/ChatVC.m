//
//  ChatVC.m
//  SwiftBack
//
//  Created by Elluminati on 04/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "ChatVC.h"
#import "MessageTableViewCell.h"
#import "SWRevealViewController.h"

typedef NS_ENUM(int, MessageDirection) { Incoming, Outgoing, };


@interface ChatVC ()<QBChatDelegate>
{
    QBChatDialog *chatDialog;
}
@end

@implementation ChatVC
@synthesize receipent,receipentDialog;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tableHeight = tableForChat.frame.size.height;
    viewPosition = viewForSender.frame.origin.y;
    
    [[QBChat instance] addDelegate:self];
    arrMessages = [[NSMutableArray alloc]init];
    [self customSetup];
    [self createChatDialog];
    
    if(chatDialog.name.length == 0 || chatDialog.name == (id)[NSNull null]) {
        [self.lblNameAtNavBar setText:[NSString stringWithFormat:@"%@",receipent.fullName]];
        
    } else {
        [self.lblNameAtNavBar setText:[NSString stringWithFormat:@"%@",chatDialog.name]];
    }

    
    dictLogin = [USERDEFAULT objectForKey:PREF_LOGIN_OBJECT];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];


    _chatBtn.layer.cornerRadius = 5;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)createChatDialog{
    
    [APPDELEGATE showLoadingWithTitle:@"Loading"];
    chatDialog = [[QBChatDialog alloc] initWithDialogID:NULL  type:QBChatDialogTypePrivate];
    
    if (self.type == 1) {
        [chatDialog setOccupantIDs:@[[NSNumber numberWithInteger:self.receipent.ID]]];

        [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
            if (response) {
                
                chatDialog = createdDialog;
                [self loadMessages];
                [self getEmailOfWalker];
            }
        } errorBlock:^(QBResponse *response) {
            [APPDELEGATE hideLoadingView];

        }];
    }
    else{
        chatDialog = self.receipentDialog;
        [self loadMessages];

    }

}

-(void)loadMessages{
    
    
    QBResponsePage *resPage = [QBResponsePage responsePageWithLimit:1000 skip:0];
    
    [QBRequest messagesWithDialogID:[NSString stringWithFormat:@"%@",chatDialog.ID] extendedRequest:nil forPage:resPage successBlock:^(QBResponse *response, NSArray *messages, QBResponsePage *responcePage) {
        
        if ([messages  count]> 0) {
            [arrMessages addObjectsFromArray:messages];
            [tableForChat reloadData];
            [self scrollToBottom];
        }
        [self getEmailOfWalker];

    } errorBlock:^(QBResponse *response) {
        NSLog(@"error: %@", response.error);
        [APPDELEGATE hideLoadingView];
    }];

}

-(void)getEmailOfWalker{
    
    if (self.type) {
        walkerEmail = self.receipent.email;
        [APPDELEGATE hideLoadingView];

    }
    else{
        [APPDELEGATE showLoadingWithTitle:@"Loading"];
        [QBRequest usersWithFullName:chatDialog.name successBlock:^(QBResponse * _Nonnull response, QBGeneralResponsePage * _Nullable page, NSArray<QBUUser *> * _Nullable users) {
            
            
            [APPDELEGATE hideLoadingView];
            
            receipent = [users objectAtIndex:0];
            walkerEmail = receipent.email;
            
        } errorBlock:^(QBResponse * _Nonnull response) {
            [APPDELEGATE hideLoadingView];
            
        }];

        
    }
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    [tableForChat setFrame:CGRectMake(tableForChat.frame.origin.x, tableForChat.frame.origin.y, tableForChat.frame.size.width, tableForChat.frame.size.height - (keyboardSize.height + viewForSender.frame.size.height-45))];
    
    
    [viewForSender setFrame:CGRectMake(viewForSender.frame.origin.x, tableForChat.frame.origin.y+tableForChat.frame.size.height, viewForSender.frame.size.width, viewForSender.frame.size.height)];
    
    [self scrollToBottom];
    
    
      //  [txtMessage setFrame:CGRectMake(0, self.view.frame.size.height - keyboardSize.height - self.viewForAdditionalPassenger.frame.size.height, self.viewForAdditionalPassenger.frame.size.width, self.viewForAdditionalPassenger.frame.size.height)];
    
    //Given size may not account for screen rotation
    
    
    //your other code here..........
}

- (void)keyboardWasHidden:(NSNotification *)notification
{
    
    [tableForChat setFrame:CGRectMake(tableForChat.frame.origin.x, tableForChat.frame.origin.y, tableForChat.frame.size.width, tableHeight)];
    
    [viewForSender setFrame:CGRectMake(viewForSender.frame.origin.x, viewPosition, viewForSender.frame.size.width, viewForSender.frame.size.height)];
    
    //your other code here..........
}



- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QBChatMessage *message = [arrMessages objectAtIndex:[indexPath row]];
    MessageTableViewCell *cell;
    if (message.recipientID == chatDialog.userID) {
        cell = [self dequeOrLoadMessageTableViewCell:1];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[dictLogin objectForKey:PARAM_FIRST_NAME],[dictLogin objectForKey:PARAM_LAST_NAME]];
        [cell.nameLabel setTextColor: [UIColor darkGrayColor]];
        [cell.message setTextAlignment:NSTextAlignmentRight];

        
    }
    else{
        cell = [self dequeOrLoadMessageTableViewCell:0];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@",chatDialog.name];
        [cell.nameLabel setTextColor: [UIColor darkGrayColor]];
        [cell.message setTextAlignment:NSTextAlignmentLeft];

        
        
    }
    
    
    cell.message.text = message.text;
    return cell;
}

- (MessageTableViewCell *)dequeOrLoadMessageTableViewCell:(MessageDirection)direction {
    
    NSString *identifier =
    [NSString stringWithFormat:@"%@MessageCell", (Incoming == direction) ? @"Incoming" : @"Outgoing"];
    
    MessageTableViewCell *cell = [tableForChat dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil][0];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

#pragma mark -

/*
 Scrolls the message view to the bottom to ensure we always see the latest message.
 */
- (void)scrollToBottom {
    
    if ([arrMessages  count] > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(arrMessages.count - 1)inSection:0];
        
        [tableForChat scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [tableForChat setFrame:CGRectMake(tableForChat.frame.origin.x, tableForChat.frame.origin.y, tableForChat.frame.size.width, tableHeight)];
    
    [viewForSender setFrame:CGRectMake(viewForSender.frame.origin.x, viewPosition, viewForSender.frame.size.width, viewForSender.frame.size.height)];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - QMChatServiceDelegate

-(void)chatDidDeliverMessageWithID:(NSString *)messageID dialogID:(NSString *)dialogID toUserID:(NSUInteger)userID{
  /*  UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Success"
                                  message:@"Message Sent"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    
                                    
                                }];
    
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil]; */
}


//- (void)chatService:(QMChatService *)chatService didUpdateMessage:(QBChatMessage *)message forDialogID:(NSString *)dialogID
//{
//    
//}


-(void)chatDidReceiveMessage:(QBChatMessage *)message{
    
    [arrMessages addObject:message];
    [tableForChat reloadData];
    [self scrollToBottom];
}

- (IBAction)onClickSend:(id)sender {
    
    if ([APPDELEGATE connected]) {
        if ([txtMessage.text length] > 0) {
            [self.chatBtn setUserInteractionEnabled:NO];
            QBChatMessage *message = [QBChatMessage message];
            [message setText:txtMessage.text];
            [message setRecipientID:self.receipent.ID];
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            params[@"save_to_history"] = @YES;
            [message setCustomParameters:params];
            //
            [chatDialog sendMessage:message completionBlock:^(NSError * _Nullable error) {
                
                if (!error) {
                    [self.chatBtn setUserInteractionEnabled:YES];
                    [txtMessage setText:@""];
                    [arrMessages addObject:message];
                    [tableForChat reloadData];
                    [self scrollToBottom];
                    [self pushToDriver:message.text];

                }
            }];
        }
        else{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please enter message", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
            [txtMessage resignFirstResponder];
        }

    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)pushToDriver:(NSString*)message{
    
    if ([APPDELEGATE connected]) {
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        
        if ([walkerEmail length] > 1) {
            [dictParam setObject:walkerEmail forKey:@"provider_email"];
            
        }
        else{
            [self getEmailOfWalker];
            return;
        }
        
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:message forKey:@"message"];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_SEND_PUSH_TO_WALKER withParamData:dictParam withBlock:^(id response, NSError *error) {
            if (response) {
                
            }
        }];
    }
}

- (IBAction)onClickBack:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}
@end
