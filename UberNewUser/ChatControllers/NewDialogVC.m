//
//  NewDialogVC.m
//  SwiftBack
//
//  Created by Elluminati on 04/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "NewDialogVC.h"
#import "ServicesManager.h"
#import "ChatVC.h"
#import "SWRevealViewController.h"

@interface NewDialogVC ()

@end

@implementation NewDialogVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrQBWalkers = [[NSMutableArray alloc]init];
    [self customSetup];
    [self downloadLatestUsers];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)downloadLatestUsers
{
    [APPDELEGATE showLoadingWithTitle:@"Loading"];

    [QBRequest usersWithTags:[NSArray arrayWithObjects:@"SwiftbackDriver", nil] successBlock:^(QBResponse * _Nonnull response, QBGeneralResponsePage * _Nullable page, NSArray<QBUUser *> * _Nullable users) {
        
        if (response) {
            if (users) {
                [APPDELEGATE hideLoadingView];
                NSMutableArray *tempDrivers = [users mutableCopy];
                
                for (int d=0; d<[tempDrivers count]; d++) {
                    
                    QBUUser *user = [tempDrivers objectAtIndex:d];
                    
                    for (NSDictionary *dict in arrWalkers) {
                        
                        if ([[dict objectForKey:PARAM_NAME]isEqualToString:[user fullName]]) {
                            if (![arrQBWalkers containsObject:user]) {
                                [arrQBWalkers addObject:user];

                            }
                            
                        }
                        
                    }
                    
                }
                [tableForNewDialogs setHidden:NO];
                [tableForNewDialogs reloadData];
                
            }
        }
    } errorBlock:^(QBResponse * _Nonnull response) {
        
    }];
    
   
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrQBWalkers count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    [cell.textLabel setText:[(QBUUser*)[arrQBWalkers objectAtIndex:indexPath.row] fullName] ];
    [cell.textLabel setTextColor: [UIColor darkGrayColor]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    QBUUser *user = [arrQBWalkers objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"segueToChatVC" sender:user];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"segueToChatVC"]) {
        ChatVC *chat = [segue destinationViewController];
        chat.receipent = sender;
        chat.type = 1;
    }
   
}


- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
