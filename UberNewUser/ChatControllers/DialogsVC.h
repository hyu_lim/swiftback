//
//  DialogsVC.h
//  SwiftBack
//
//  Created by Elluminati on 04/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialogsVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrQBDialogs;
    
    NSTimer *timer;
    
    __weak IBOutlet UIButton *menuBtn;
    
    __weak IBOutlet UITableView *tableForDialogs;
}

- (IBAction)onClickBack:(id)sender;

@end
