//
//  DisplayCardVC.h
//  UberforXOwner
//
//  Created by Deep Gami on 17/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "SWTableViewCell.h"

@interface DisplayCardVC : BaseVC<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>
{
  NSIndexPath *cellIndexPath;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNavigation;
@property (weak, nonatomic) IBOutlet UILabel *lblNoCards;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoItems;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)addCardBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;

@end
