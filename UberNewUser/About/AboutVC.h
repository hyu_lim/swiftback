//
//  AboutVC.h
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface AboutVC : BaseVC

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnTutorial;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewAbout;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIImageView *imgSwift;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic,strong) NSMutableArray *arrInformation;
@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)onClickTutorial:(id)sender;

@end
