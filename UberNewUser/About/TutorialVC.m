//
//  TutorialVC.m
//  SwiftBack
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "TutorialVC.h"


@interface TutorialVC ()
{
    int i;
}
@end

@implementation TutorialVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    i=1;
    self.img.image=[UIImage imageNamed:@"SB_P1.png"];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
}


-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
   if(i>1)
   {
       i--;
       self.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"SB_P%d.png",i]];
   }
   else
   {
       i=1;
       self.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"SB_P%d.png",i]];
   }
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(i<4)
    {
        i++;
        self.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"SB_P%d.png",i]];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
