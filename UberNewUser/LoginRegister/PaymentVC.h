//
//  PaymentVC.h
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "CardIOPaymentViewControllerDelegate.h"
#import "PTKView.h"

#import <UIKit/UIKit.h>
#import "PaymentPageContentViewController.h"

@protocol STPPaymentCardTextFieldDelegate <NSObject>
@end
@interface PaymentVC : BaseVC<UITextFieldDelegate,UIAlertViewDelegate,UIPageViewControllerDataSource>
{
    
}

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages2;
@property (strong, nonatomic) NSArray *pageTitles3;

@property(nonatomic,weak)IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;

///////// Actions


- (IBAction)scanBtnPressed:(id)sender;
- (IBAction)addPaymentBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)STPPaymentCardTextField:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;



///////// Property
@property (nonatomic,strong) NSString *strForPayment;
@property (nonatomic,strong) NSString *strForID;
@property (nonatomic,strong) NSString *strForToken;


///// Outlets

@property (weak, nonatomic) IBOutlet UIButton *btnAddPayment;
@property(weak, nonatomic) PTKView *paymentView;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
- (IBAction)onClickBackButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) id <STPPaymentCardTextFieldDelegate> delegate;


@property (weak, nonatomic) IBOutlet UIButton *skipBtn;
@property (weak, nonatomic) IBOutlet UIView *blurView;

@end
