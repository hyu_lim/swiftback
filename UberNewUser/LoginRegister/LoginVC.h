//
//  LoginVC.h
//  Uber
//
//  Developed by Elluminati on 21/06/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "BaseVC.h"
#import "Reachability.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginVC : BaseVC<UITextFieldDelegate,UIGestureRecognizerDelegate,FBSDKLoginButtonDelegate>
{
    
}

@property NetworkStatus internetConnectionStatus;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPsw;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnFB;

@property(nonatomic,weak) IBOutlet UITextField *txtEmail;
@property  (nonatomic,weak) IBOutlet UITextField *txtPsw;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,weak) IBOutlet UIScrollView *scrLogin;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UILabel *lblButtonsBackground;

- (IBAction)onClickGooglePlus:(id)sender;
- (IBAction)onClickFacebook:(id)sender;
- (IBAction)onClickLogin:(id)sender;
- (IBAction)onClickForgotPsw:(id)sender;
- (IBAction)onClickBack:(id)sender;

@end
