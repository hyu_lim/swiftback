//
//  OTPVC.h
//  SwiftBack
//
//  Created by My Mac on 10/30/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPVC : UIViewController<UITextFieldDelegate>
{
    
    __weak IBOutlet UILabel *lblMobileNo;
    
    __weak IBOutlet UITextField *txtOTP;
    __weak IBOutlet UILabel *lblTimer;
    
    
    NSMutableDictionary *dictLogin;
    
    NSTimer *timerForOTPExpiry;
    
    int secondsLeft;
    
    int minutes, seconds;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
- (IBAction)onClickSubmit:(id)sender;
- (IBAction)onClickResendOTP:(id)sender;
- (IBAction)onClickBack:(id)sender;
@end
