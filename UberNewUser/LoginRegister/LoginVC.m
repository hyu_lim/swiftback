//
//  LoginVC.m
//  Uber
//
//  Created by Elluminati - macbook on 21/06/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "LoginVC.h"
#import "FacebookUtility.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "UtilityClass.h"
#import "UberStyleGuide.h"


@interface LoginVC ()
{
    NSString *strForSocialId,*strLoginType,*strForEmail;
    AppDelegate *appDelegate;
    
}

@end

@implementation LoginVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}



#pragma mark -
#pragma mark - ViewLife Cycle

//Scroll View parameters
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:CGSizeMake(320, 600)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ////UI STYLE////
    
    //facebook button
    _btnFB.layer.cornerRadius = 5;
    _btnFB.layer.borderWidth = 1;
    _btnFB.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //register button
    _btnSignIn.layer.cornerRadius = 5;
    _btnSignIn.layer.borderWidth = 1;
    _btnSignIn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    // background layer of "Sign in with Facebook" and "Sign in" button
    _lblButtonsBackground.layer.cornerRadius = 10;
    _lblButtonsBackground.clipsToBounds = YES;
    _lblButtonsBackground.layer.borderWidth = 0.7;
    _lblButtonsBackground.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
    
    //[self.txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //[self.txtPsw setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    //self.txtEmail.font=[UberStyleGuide fontRegular];
    //self.txtPsw.font=[UberStyleGuide fontRegular];
    
    //self.btnSignIn=[APPDELEGATE setBoldFontDiscriptor:self.btnSignIn];
    //self.btnForgotPsw=[APPDELEGATE setBoldFontDiscriptor:self.btnForgotPsw];
    //self.btnSignUp=[APPDELEGATE setBoldFontDiscriptor:self.btnSignUp];
    //self.lblTitle.text=NSLocalizedString(@"SIGN IN", nil);
    /*self.txtEmail.text=@"deep.gami077@gmail.com";
     self.txtPsw.text=@"123123";*/
    
    strLoginType=@"manual";
    
    [self setLocalization];
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.readPermissions =@[@"email",@"user_friends",@"public_profile"];
    loginButton.delegate=self;
    // Optional: Place the button in the center of your view.
    self.btnFB = loginButton;
    
    //[super setNavBarTitle:NSLocalizedString(@"SIGN IN", nil)];
   // [super setBackBarItem];
    
    
    //[self performSegueWithIdentifier:SEGUE_SUCCESS_LOGIN sender:self];
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTapGestureRecognizer];

}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

-(void)viewWillAppear:(BOOL)animated
{
  //  [[FacebookUtility sharedObject] logOutFromFacebook];
    self.navigationController.navigationBarHidden=YES;
}
/*
 -(void)viewWillAppear:(BOOL)animated
 {
 [super viewWillAppear:animated];
 self.navigationController.navigationBarHidden=YES;
 }
 
 -(void)viewWillDisappear:(BOOL)animated
 {
 self.navigationController.navigationBarHidden=NO;
 [super viewWillDisappear:animated];
 }
 */
-(void)setLocalization
{
    self.txtEmail.placeholder=NSLocalizedString(@"Email", nil);
    self.txtPsw.placeholder=NSLocalizedString(@"Password", nil);
    [self.btnForgotPsw setTitle:NSLocalizedString(@"Forgot Password", nil) forState:UIControlStateNormal];
    [self.btnSignIn setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
}
- (void)viewDidAppear:(BOOL)animated
{
     [self.btnSignUp setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    
}

/*#pragma mark -
#pragma mark - Actions

- (IBAction)onClickGooglePlus:(id)sender
{
    
    
    if ([[GooglePlusUtility sharedObject]isLogin])
    {
        [[GooglePlusUtility sharedObject]loginWithBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response) {
                 strLoginType=@"google";

                 NSLog(@"Gmail Response ->%@ ",response);
                 strForSocialId=[response valueForKey:@"userid"];
                 strForEmail=[response valueForKey:@"email"];
                 self.txtEmail.text=strForEmail;
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 [self onClickLogin:nil];
                 
             }
         }];
    }
    else
    {
        [[GooglePlusUtility sharedObject]loginWithBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response) {
                 NSLog(@"Gmail Response ->%@ ",response);
                 strForSocialId=[response valueForKey:@"userid"];
                 strForEmail=[response valueForKey:@"email"];
                 self.txtEmail.text=strForEmail;
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 [self onClickLogin:nil];
                 
             }
         }];
    }

}
*/
- (IBAction)onClickFacebook:(id)sender
{
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        [APPDELEGATE hideLoadingView];

        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        } else {
            NSLog(@"Logged in");
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];

            if ([FBSDKAccessToken currentAccessToken]) {
                
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                              initWithGraphPath:@"me"
                                              parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
                                              HTTPMethod:@"GET"];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                      id result,
                                                      NSError *error) {
                    NSLog(@"%@",result);
                    // Handle the result
                    strLoginType=@"facebook";

                    strForSocialId=[result valueForKey:@"id"];
                    strForEmail=[result valueForKey:@"email"];
                    self.txtEmail.text=strForEmail;
                    [[AppDelegate sharedAppDelegate]hideLoadingView];
                    [self.btnFB setHidden:YES];
                    [self onClickLogin:nil];
                    
                }];
            }
        }
    }];
    
}

//    if (![[FacebookUtility sharedObject]isLogin])
//    {
//        [[FacebookUtility sharedObject]loginInFacebook:^(BOOL success, NSError *error)
//         {
//             [APPDELEGATE hideLoadingView];
//             if (success)
//             {
//                 NSLog(@"Success");
//                 appDelegate = [UIApplication sharedApplication].delegate;
//                 [appDelegate userLoggedIn];
//                 [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error) {
//                     if (response) {
//                         strForSocialId=[response valueForKey:@"id"];
//                         strForEmail=[response valueForKey:@"email"];
//                         self.txtEmail.text=strForEmail;
//                         [[AppDelegate sharedAppDelegate]hideLoadingView];
//                         
//                         [self onClickLogin:nil];
//                         
//                     }
//                 }];
//             }
//         }];
//    }
//    else{
//        NSLog(@"User Login Click");
//        appDelegate = [UIApplication sharedApplication].delegate;
//        [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error) {
//            [APPDELEGATE hideLoadingView];
//            if (response) {
//                strForSocialId=[response valueForKey:@"id"];
//                strForEmail=[response valueForKey:@"email"];
//                self.txtEmail.text=strForEmail;
//                [[AppDelegate sharedAppDelegate]hideLoadingView];
//                
//                [self onClickLogin:nil];
//            }
//        }];
//        [appDelegate userLoggedIn];
//    }
    


-(IBAction)onClickLogin:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if(self.txtEmail.text.length>0)
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            NSString *strDeviceId=[pref objectForKey:PREF_DEVICE_TOKEN];
            
            if ([strDeviceId length] == 0 || strDeviceId == nil)
            {
                strDeviceId = @"121212121212121212";
            }
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:@"ios" forKey:PARAM_DEVICE_TYPE];
            [dictParam setValue:strDeviceId forKey:PARAM_DEVICE_TOKEN];
            if([strLoginType isEqualToString:@"manual"])
                [dictParam setValue:self.txtEmail.text forKey:PARAM_EMAIL];
            // else
            //     [dictParam setValue:strForEmail forKey:PARAM_EMAIL];
            
            [dictParam setValue:strLoginType forKey:PARAM_LOGIN_BY];
            
            if([strLoginType isEqualToString:@"facebook"])
                [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
            else if ([strLoginType isEqualToString:@"google"])
                [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
            else
                [dictParam setValue:self.txtPsw.text forKey:PARAM_PASSWORD];
            
            [dictParam setValue:@"1" forKey:@"test"];
            
            NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

            
            [dictParam setValue:version forKey:@"version"];
            
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_LOGIN withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 NSLog(@"Login Response ---> %@",response);
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         
                         NSString *strLog=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"LOGIN_SUCCESS", nil),[response valueForKey:@"first_name"]];
                         
                         [APPDELEGATE showToastMessage:strLog];
                         
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                         [pref setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                         [pref setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                         [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                         [pref setBool:YES forKey:PREF_IS_LOGIN];
                         [pref setObject:[response valueForKey:@"gender"] forKey:@"usergender"];
                         [pref synchronize];
                         
                         
                         
                         // if ([[response valueForKey:@"otp_verify"]integerValue] == 1)
                         // {
                         [self performSegueWithIdentifier:SEGUE_SUCCESS_LOGIN sender:self];
                         
                         //  }
                         // else
                         // {
                         //   [self performSegueWithIdentifier:SEGUE_TO_OTP sender:self];
                         
                         // }
                     }
                     else
                     {
                         
                         [self.btnFB setHidden:NO];

                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 
             }];
        }
        else
        {
            if(self.txtEmail.text.length==0)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_PASSWORD", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(IBAction)onClickForgotPsw:(id)sender
{
    [self textFieldShouldReturn:self.txtPsw];
    /*
    if (self.txtEmail.text.length==0)
    {
        [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:@"Enter your email id."];
        return;
    }
    else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
    {
        [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:@"Enter valid email id."];
        return;
    }
     */
}

#pragma mark -
#pragma mark - TextField Delegate
-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtEmail resignFirstResponder];
    [self.txtPsw resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=0;
    if (textField==self.txtEmail)
    {
        y=50;
    }
    else if (textField==self.txtPsw){
        y=100;
    }
    [self.scrLogin setContentOffset:CGPointMake(0, y) animated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.txtEmail)
    {
        [self.txtPsw becomeFirstResponder];
    }
    else if (textField==self.txtPsw){
        [textField resignFirstResponder];
        [self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    return YES;
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
