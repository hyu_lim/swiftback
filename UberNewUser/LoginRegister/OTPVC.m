//
//  OTPVC.m
//  SwiftBack
//
//  Created by My Mac on 10/30/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "OTPVC.h"

@interface OTPVC ()

@end

@implementation OTPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //update statusbar
    [self setNeedsStatusBarAppearanceUpdate];
    
    dictLogin  = [[USERDEFAULT objectForKey:PREF_LOGIN_OBJECT] mutableCopy];
    [lblMobileNo setText:[dictLogin objectForKey:@"phone"]];
    secondsLeft = 300.0;
    
    if ([USERDEFAULT boolForKey:@"close"])
    {
        NSDate *closeDate = [USERDEFAULT objectForKey:@"closeDate"];
        NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:closeDate];
        
        secondsLeft = secondsLeft - secondsBetween;
        [USERDEFAULT setBool:NO forKey:@"close"];
        [USERDEFAULT synchronize];
    }
    
    timerForOTPExpiry = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];


    
    // Do any additional setup after loading the view.
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
}



//UPDATE STATUS BAR
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
//

-(void)appWillResignActive:(NSNotification*)note
{
    
}


-(void)appWillTerminate:(NSNotification*)note
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    [USERDEFAULT setBool:YES forKey:@"close"];
    [USERDEFAULT synchronize];
    
}



-(void)updateTimer
{
    if (secondsLeft > 0)
    {
        secondsLeft -- ;
        minutes = (secondsLeft % 3600) / 60;
        seconds = (secondsLeft %3600) % 60;
        lblTimer.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
        
    }
    else
    {
        [timerForOTPExpiry invalidate];
        timerForOTPExpiry = nil;
        [USERDEFAULT removeObjectForKey:PREF_LOGIN_OBJECT];
        [USERDEFAULT synchronize];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.scrollView setContentOffset:offset animated:YES];
    
    return [textField resignFirstResponder];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtOTP resignFirstResponder];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickSubmit:(id)sender {
    
    if ([APPDELEGATE connected])
    {
        if ([[[UtilityClass sharedObject] trimString:txtOTP.text] length] < 1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter OTP" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        else
        {
            [APPDELEGATE showLoadingWithTitle:@"Please Wait..."];
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            [dictParam setObject:txtOTP.text forKey:PARAM_OTP_CODE];
            [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [helper getDataFromPath:FILE_VERIFY_USER withParamData:dictParam withBlock:^(id response, NSError *error) {
                [APPDELEGATE hideLoadingView];
                if (response)
                {
                    NSLog(@"Verify %@",response);
                    if ([[response valueForKey:@"success"]boolValue])
                    {
                        [dictLogin setObject:@"1" forKey:@"otp_verify"];
                        
                        [USERDEFAULT setObject:dictLogin forKey:PREF_LOGIN_OBJECT];
                        [USERDEFAULT synchronize];
                        [self performSegueWithIdentifier:SEGUE_TO_APPLY_REFERRAL_CODE sender:nil];
                        //[self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    else
                    {
                        [APPDELEGATE showToastMessage:[response objectForKey:@"error"]];
                    }
                }
            }];
        }
    }
}

- (IBAction)onClickResendOTP:(id)sender {
    
    
    if ([APPDELEGATE connected])
    {
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        NSString *mail =   [USERDEFAULT objectForKey:PARAM_EMAIL];
        if ([mail length] > 0)
        {
            [dictParam setObject:mail forKey:PARAM_EMAIL];
            
        }
        else
        {
            [dictParam setObject:[dictLogin objectForKey:PARAM_EMAIL] forKey:PARAM_EMAIL];
            
        }
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_RESEND_OTP withParamData:dictLogin withBlock:^(id response, NSError *error) {
            if (response)
            {
                if ([[response valueForKey:@"success"]boolValue])
                {
                    [APPDELEGATE showToastMessage:@"OTP has been sent successfully."];
                }
            }
        }];
    }
    
    
}

- (IBAction)onClickBack:(id)sender {
    
   // [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark- TextField Delegate



//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.txtCode resignFirstResponder];
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    if(textField==self->txtOTP)
    {
        offset=CGPointMake(0, 200);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    //self.viewForReferralError.hidden=YES;
}

//self scrolling page view controller



@end
