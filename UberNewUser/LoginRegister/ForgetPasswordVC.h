//
//  ForgetPasswordVC.h
//  UberforXOwner
//
//  Created by Deep Gami on 14/11/14.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "BaseVC.h"

@interface ForgetPasswordVC : BaseVC <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
- (IBAction)signBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
- (IBAction)onClickBackBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIView *scrLogin;

@end
