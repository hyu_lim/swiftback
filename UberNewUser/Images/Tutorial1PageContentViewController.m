//
//  Tutorial1PageContentViewController.m
//  SwiftBack
//
//  Created by Hengyu Lim on 11/20/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "Tutorial1PageContentViewController.h"

#import "PageContentViewController.h"

@interface Tutorial1PageContentViewController ()
{
int i;
}
    @end

@implementation Tutorial1PageContentViewController

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    
    [super viewDidLoad];
    // Create the data model
    _pageTitles = @[@"Welcome to", @"Begin with a", @"Get Notified", @"Enjoy up to 2X"];
    _pageTitles2 = @[@"SwiftBack!", @"Ride Request", @"about your ride", @"Cheaper rates!"];
    _pageImages = @[@"test_tut1.png", @"test_tut2.png", @"test_tut3.png", @"test_tut4.png"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    i=1;
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    //page controller properties
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    //    if ([USERDEFAULT boolForKey:@"tutorial"])
    //    {
    //        if([USERDEFAULT boolForKey:PREF_IS_LOGIN]) {
    //            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
    //            self.navigationController.navigationBarHidden=YES;
    //            [self performSegueWithIdentifier:SEGUE_TO_DIRECT_LOGIN sender:self];
    //            [APPDELEGATE hideLoadingView];
    //        }
    //
    //    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)startWalkthrough:(id)sender {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    if (([self.pageTitles2 count] == 0) || (index >= [self.pageTitles2 count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.titleText2 = self.pageTitles2[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

// Redirect after complete

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
        [self.navigationController popToRootViewControllerAnimated:YES];
    
}

//Back Button

- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
    
    if (index == [self.pageTitles2 count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}



- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
    return [self.pageTitles2 count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
- (IBAction)onClickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end
