//
//  PromoCodeVC.m
//  SwiftBack Driver
//
//  Created by My Mac on 10/2/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import "PromoCodeVC.h"
#import  "SWRevealViewController.h"
#import "Social/Social.h"
#import "SWRevealViewController.h"

@interface PromoCodeVC ()

@property (nonatomic, strong) SLComposeViewController *facebookComposerSheet;
@end

@implementation PromoCodeVC



- (void)viewDidLoad {
    [super viewDidLoad];
    [super setBackBarItem];
    [self customSetup];
    // Do any additional setup after loading the view.
    
    //rectangle background
    backgroundRect.layer.cornerRadius = 10;
    backgroundRect.layer.borderWidth = 1;
    backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    backgroundRect.layer.shadowRadius = 5.0;
    backgroundRect.layer.shadowOpacity = 0.4;
    
    //rectangle background2
    backgroundRect2.layer.cornerRadius = 10;
    backgroundRect2.layer.borderWidth = 1;
    backgroundRect2.layer.borderColor = [UIColor whiteColor].CGColor;
    backgroundRect2.layer.shadowRadius = 5.0;
    backgroundRect2.layer.shadowOpacity = 0.4;
    
    //rectangle background
    _headerRect.layer.cornerRadius = 10;
    _headerRect.layer.borderWidth = 2;
    _headerRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _headerRect.layer.shadowRadius = 5.0;
    _headerRect.layer.shadowOpacity = 0.4;
    
    //rectangle backgroundshare
    backgroundRectShare.layer.cornerRadius = 10;
    backgroundRectShare.layer.borderWidth = 2;
    backgroundRectShare.layer.borderColor = [UIColor whiteColor].CGColor;
    backgroundRectShare.layer.shadowRadius = 5.0;
    backgroundRectShare.layer.shadowOpacity = 0.4;
    
    //btn Share on Facebook
    _btnShare.layer.cornerRadius = 5;
    _btnShare.layer.borderWidth = 1;
    _btnShare.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //btn Submit
    //btn Share on FAcebook
    //facebook button
    _btnSubmit.layer.cornerRadius = 5;
    _btnSubmit.layer.borderWidth = 1;
    _btnSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
    
//    //View Controller Clipping
//    _mainView.layer.cornerRadius = 5;
//    _mainView.clipsToBounds = YES;
    
    
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    
    NSString *urlString = @"http://swiftback.com/promo.php";
    NSError  *error     = nil;
    NSData   *dataURL   = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString] options:kNilOptions error:&error];
    
    if (error)
        NSLog(@"%s: dataWithContentsOfURL error: %@", __FUNCTION__, error);
    else
    {
        NSString *result = [[NSString alloc] initWithData:dataURL encoding:NSUTF8StringEncoding];
        NSLog(@"%s: result = %@", __FUNCTION__, result);
        
        // if you were updating a label with an `IBOutlet` called `resultLabel`, you'd do something like:
        
        self.pushHeadline.text = result;
    }
    
    NSString *urlString2 = @"http://swiftback.com/promodeadline.php";
    NSError  *error2     = nil;
    NSData   *dataURL2   = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2] options:kNilOptions error:&error];
    
    if (error2)
        NSLog(@"%s: dataWithContentsOfURL error: %@", __FUNCTION__, error);
    else
    {
        NSString *result = [[NSString alloc] initWithData:dataURL2 encoding:NSUTF8StringEncoding];
        NSLog(@"%s: result = %@", __FUNCTION__, result);
        
        // if you were updating a label with an `IBOutlet` called `resultLabel`, you'd do something like:
        
        self.promoDeadline.text = result;
    }
    
    //self.pushHeadline.text=[NSString stringWithFormat:@"%@", [dictInfo valueForKey:@"first_name"]];
    
    //she promotion with user referral
    self.userReferralCode.text=[NSString stringWithFormat:@"%@'s Referral Code", [dictInfo valueForKey:@"first_name"]];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
        [viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    if(textField==self.txtPromoCode)
    {
        offset=CGPointMake(0, 170);
        [self.viewForApply setContentOffset:offset animated:YES];
    }
    //self.viewForReferralError.hidden=YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtPromoCode resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.viewForApply setContentOffset:offset animated:YES];
    
    return [textField resignFirstResponder];
    
}

-(void)getReferralCode
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please Wait"];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_REFERRAL,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [viewForReferral setHidden:NO];
                     [self.viewForApply setHidden:YES];
                     [pref setObject:[response valueForKey:@"referral_code"] forKey:PREF_REFERRAL_CODE];
                     [pref synchronize];
                     lblReferralCode.text=[response valueForKey:@"referral_code"];
                 }
                 else
                 {}
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
}

- (IBAction)shareReferral:(id)sender {
    
    self.facebookComposerSheet = [[SLComposeViewController alloc] init];
    self.facebookComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    NSString *composerText = [NSString stringWithFormat:@"Check out SwiftBack,the greener and affodable alternative to taxis you need,use my promo code to get a bonus discount off the cheaper ride in town. Limited to first 5 only."];
    [self.facebookComposerSheet setInitialText:composerText];
    [self.facebookComposerSheet addImage:[self takeScreenshot]];
    UIViewController *root = self.view.window.rootViewController;
    [root presentViewController:self.facebookComposerSheet animated:YES completion:nil ];
    [_iconShare setHidden:YES];
    [_textShare setHidden:NO];
    [backgroundRect setHidden:YES];
    [_userReferralCode setHidden:YES];
    [_useReferralCode setHidden:NO];
    [_hideTitle setHidden:YES];
    [_hideHeader setHidden:YES];
    [_hideLogo setHidden:YES];
    [backgroundRectShare setHidden:YES];
    
}

// Utility function for taking a screenshot
- (UIImage*)takeScreenshot
{
    
    [self.iconShare setHidden:NO];
    [_textShare setHidden:YES];
    [backgroundRect setHidden:YES];
    [_hideTitle setHidden:NO];
    [_hideHeader setHidden:NO];
    [_hideLogo setHidden:NO];
    [backgroundRectShare setHidden:NO];
    [_userReferralCode setHidden:NO];
    [_useReferralCode setHidden:YES];
    
    
    //first we will make an UIImage from your view
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *sourceImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //now we will position the image, X/Y away from top left corner to get the portion we want
    UIGraphicsBeginImageContext(viewForScreenshot.frame.size);
    [sourceImage drawAtPoint:CGPointMake(0, -90)];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

- (IBAction)onClickReferPromo:(id)sender {
    
    [viewForReferral setHidden:YES];
    [self.viewForApply setHidden:NO];
    [backgroundRect setHidden:NO];
}

- (IBAction)onClickReferralCode:(id)sender {
    
    [self getReferralCode];
    [self.viewForApply setHidden:YES];
    [backgroundRect setHidden:YES];
    [backgroundRectShare setHidden:YES];
    [backgroundRect2 setHidden:NO];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.viewForApply setContentSize:CGSizeMake(320, 482)];
}

/*-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtOTP resignFirstResponder];
}
*/

- (IBAction)onClickSubmit:(id)sender {
    
    if ([APPDELEGATE connected])
    {
        if ([[[UtilityClass sharedObject] trimString:self.txtPromoCode.text] length ]< 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Enter Promocode", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"Applying Promocode", nil)];
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:self.txtPromoCode.text forKey:PARAM_REFERRAL_CODE];
            
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [helper getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error) {
                [APPDELEGATE hideLoadingView];
                if (response)
                {
                    if ([[response objectForKey:@"success"]boolValue])
                    {
                      //  [APPDELEGATE showToastMessage:NSLocalizedString(@"PromoCode Applied Successfully.", nil)];
                        
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PromoCode Applied Successfully.", nil) delegate:self cancelButtonTitle:@"" otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                    else
                    {
                        //[APPDELEGATE showToastMessage:[response objectForKey:@"error"]];
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response objectForKey:@"error"] delegate:self cancelButtonTitle:@"" otherButtonTitles:@"OK", nil];
                        [alert show];

                    }
                }
            }];
        }
    }
}
@end
