//
//  CellSlider.m
//  UberNewUser
//
//  Developed by Elluminati on 30/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "CellSlider.h"

@implementation CellSlider

#pragma mark -
#pragma mark - Init

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}



- (void)awakeFromNib
{
    // Initialization code
    self.lblName.font = [UberStyleGuide fontSemiBold];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[UIColor colorWithRed:0.09 green:0.63 blue:0.52 alpha:1.0]]; // set color here
    [self setSelectedBackgroundView:selectedBackgroundView];
}

#pragma mark -
#pragma mark - Methods

-(void)setCellData:(id)data withParent:(id)parent
{
    cellData=data;
    cellParent=parent;
	
	self.lblName.font=[UIFont fontWithName:@"OpenSans-Semibold" size:16.0f];
	self.lblName.text=[cellData uppercaseString];
	self.imgIcon.image=[UIImage imageNamed:[NSString stringWithFormat:@"ub__nav_%@",cellData]];
	
}

@end
