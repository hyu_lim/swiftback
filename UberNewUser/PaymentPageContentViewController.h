//
//  PaymentPageContentViewController.h
//  SwiftBack
//
//  Created by Hengyu Lim on 11/14/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentPageContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView2;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel2;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel3;


@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;
@property NSString *titleText2;
@property NSString *titleText3;
@property NSString *imageFile2;
@end