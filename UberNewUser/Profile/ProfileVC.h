//
//  ProfileVC.h
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"

@interface ProfileVC : BaseVC <UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>


@property (weak, nonatomic) IBOutlet UILabel *userName;

@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
- (IBAction)Logout:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *backgroundReset;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
- (IBAction)selectPhotoBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *proPicImgv;
@property (weak, nonatomic) IBOutlet UIImageView *proPicImgv2;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollObj;
@property (weak, nonatomic) IBOutlet UIButton *btnProPic;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPWD;
//@property (weak, nonatomic) IBOutlet UITextField *txtNewPWD;
@property (weak, nonatomic) IBOutlet UITextField *txtConformPWD;

- (IBAction)updateBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
- (IBAction)editBtnPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
- (IBAction)btnMenuClick:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *mainView;
- (IBAction)btnEmailInfoClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForResetPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)onClickResetPassword:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForEmailInfo;
- (IBAction)onClickResetOfPasswordView:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwd;
- (IBAction)onClickBtnCancelOfPasswordView:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIView *viewForGreenOverlay;
@property (weak, nonatomic) IBOutlet UIView *blurView;

@end
