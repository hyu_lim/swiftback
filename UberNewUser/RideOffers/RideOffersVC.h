//
//  RideOffersVC.h
//  SwiftBack
//
//  Created by Elluminati on 22/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RideNextCell.h"
#import <NYSegmentedControl/NYSegmentedControl.h>

@interface RideOffersVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
	NSMutableArray *arrOffers;
    
	__weak IBOutlet UITableView *tableForOffers;
	
	__weak IBOutlet UISegmentedControl *offersSegment;
	
	__weak IBOutlet UIButton *confirmBtn;
	
	__weak IBOutlet UIButton *menuBtn;
	
	__weak IBOutlet UIImageView *no_items;
}

@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;

- (IBAction)onClickConfirmOffers:(id)sender;
- (IBAction)onClickSegment:(id)sender;

@end
