//
//  RideOffersVC.m
//  SwiftBack
//
//  Created by Elluminati on 22/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "RideOffersVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "AdvancePickupVC.h"

@interface RideOffersVC ()

@end

@implementation RideOffersVC

- (void)viewDidLoad {
	[super viewDidLoad];
	
	arrOffers = [[NSMutableArray alloc]init];
	[self customSetup];
    [self layoutSetup];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
	
	[self getOffers];
	[self confirmOfferCount];
}

-(void)confirmOfferCount
{
	NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
	[dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
	[dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
	
	AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
	[helper getDataFromPath:FILE_CONFIRM_OFFER_COUNT withParamData:dictParam withBlock:^(id response, NSError *error) {
		if (response)
		{
			if ([[response objectForKey:@"success"] boolValue])
			{
				if ([[response objectForKey:@"offer_count"] integerValue ]== 0)
				{
					[confirmBtn setTitle:@"" forState:UIControlStateNormal];
					[confirmBtn setBackgroundImage:[UIImage imageNamed:@"icon_tick_white"] forState:UIControlStateNormal];
					[confirmBtn setBackgroundColor:[UIColor clearColor]];

				}
				else
				{
					[confirmBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

					[confirmBtn setTitle:[NSString stringWithFormat:@"%ld",(long)[[response objectForKey:@"offer_count"] integerValue]] forState:UIControlStateNormal];
					[confirmBtn setBackgroundColor:[UIColor redColor]];
					
				}
			}
		}
	}];
	
	
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)customSetup
{
	SWRevealViewController *revealViewController = self.revealViewController;
	if ( revealViewController )
	{
		[menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		//Swipe to reveal menu
	}
}

- (void)layoutSetup {
    
    //confirmBtn
    confirmBtn.layer.cornerRadius = 5;
    confirmBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    confirmBtn.layer.borderWidth = 1.5f;
    
    //NYSegmentedControl
    
    self.customSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[@"Nearest", @"Earliest"]];
    self.customSegmentedControl.frame = CGRectMake(10, 60, self.view.frame.size.width - 20, 35);
    
    [self.customSegmentedControl addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
    
    self.customSegmentedControl.titleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleFont = [UIFont systemFontOfSize:12.0f];
    
    self.customSegmentedControl.backgroundColor = [UIColor colorWithRed:37.f/255.f green:131.f/255.f blue:111.f/255.f alpha:1.0f];
    
    self.customSegmentedControl.borderWidth = 0.0f;
    
    self.customSegmentedControl.segmentIndicatorBackgroundColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1.0f];
    self.customSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorInset = 2.0f;
    self.customSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    
    self.customSegmentedControl.cornerRadius = CGRectGetHeight(self.customSegmentedControl.frame) / 2.0f;
    
    [self.view addSubview:self.customSegmentedControl];
    
    self.customSegmentedControl.contentMode = UIViewContentModeScaleAspectFill;
    
    self.customSegmentedControl.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin);
    
    self.customSegmentedControl.clipsToBounds = YES;
}


 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
	 
	 if ([[segue identifier] isEqualToString:SEGUE_TO_ADVANCE_PICKUP]) {
		 AdvancePickupVC *advance = [segue destinationViewController];
		 [advance setIsOffer:@"1"];
		 [advance setDictRequest:sender];
	 }
	
 }


#pragma mark
#pragma mark - TableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arrOffers count];

}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	
	RideNextCell *cellZero = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
	
	
		NSDictionary *dictRequest = [arrOffers objectAtIndex:indexPath.row];
	
	
	UIView *viewForCellOne = [cellZero viewWithTag:904];
	viewForCellOne.layer.cornerRadius = 10;
	viewForCellOne.clipsToBounds = YES;
	
	//Adjust horizontal and vertical lines size
	UILabel *horizontalLine = [cellZero viewWithTag:908];
	UILabel *verticalLine1 = [cellZero viewWithTag:909];
	UILabel *verticalLine2 = [cellZero viewWithTag:910];
	
	horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
	
	verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
	
	verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);
	
	NSDictionary *dictWalker = [dictRequest objectForKey:@"walker"];
	
	
	[cellZero.profileImgView downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC3"]];
	[cellZero.lblName setText:[dictWalker valueForKey:@"name"]];
	
	[cellZero.profileImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
	[cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_cash"]];

	[cellZero.ratingView setUserInteractionEnabled:NO];
	
	RBRatings ratings = (float)([[dictWalker objectForKey:@"rating"] floatValue]*2);
	[cellZero.ratingView setRatings:ratings];
	

	[cellZero.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
	[cellZero.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
	[cellZero.lblCost setText:[NSString stringWithFormat:@"$%@",[dictRequest valueForKey:@"total"]]];
	[cellZero.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
	
	NSDate *date = [[UtilityClass sharedObject] stringToDate:[dictRequest valueForKey:@"request_date"] withFormate:@"yyyy-MM-dd"];
	
	
	[cellZero.lblDepartureDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
	
	[cellZero.lblDepartureTime setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"hh:MM a"]];
	
	return cellZero;
	
	
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//    return 160.0f;
	return 105.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSDictionary *dictSelected = [arrOffers objectAtIndex:indexPath.row];
	
	[self performSegueWithIdentifier:SEGUE_TO_ADVANCE_PICKUP sender:dictSelected];
}


-(void)getOffers
{
	if ([APPDELEGATE connected]) {
		
		[APPDELEGATE showLoadingWithTitle:@"Loading"];
		NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
		[dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
		[dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
		[dictParam setObject:strForCurLatitude forKey:PARAM_LATITUDE];
		[dictParam setObject:strForCurLongitude forKey:PARAM_LONGITUDE];
        
//		[dictParam setObject:[NSString stringWithFormat:@"%ld",(long)offersSegment.selectedSegmentIndex] forKey:@"nearest"];
        
        [dictParam setObject:[NSString stringWithFormat:@"%ld",(long)self.customSegmentedControl.selectedSegmentIndex] forKey:@"nearest"];
		
		AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		[helper getDataFromPath:FILE_DRIVER_OFFERS withParamData:dictParam withBlock:^(id response, NSError *error) {
			
			[APPDELEGATE hideLoadingView];
			if (response) {
				if ([[response  objectForKey:@"success"] boolValue]) {
					
					arrOffers = [[response valueForKey:@"offers"] mutableCopy];
					
					if ([arrOffers count] > 0) {
						
						[tableForOffers setHidden:NO];
						[no_items setHidden:YES];
						[tableForOffers reloadData];
					}
					else{
						[tableForOffers setHidden:YES];
						[no_items setHidden:NO];
					}
				}
				else{
					UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Ride Offers" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
					[alert show];
					[arrOffers removeAllObjects];
					[tableForOffers setHidden:YES];
					[no_items setHidden:NO];
				}
			}
			else{
				UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Ride Offers" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
				[alert show];
				[arrOffers removeAllObjects];
				[tableForOffers setHidden:YES];
				[no_items setHidden:NO];
			}
		}];
		
	}
	
}

- (IBAction)onClickConfirmOffers:(id)sender {
	
	[self performSegueWithIdentifier:SEGUE_TO_CONFIRM_OFFERS sender:nil];
	
}

- (IBAction)onClickSegment:(id)sender {
	
	[self getOffers];
}

- (void)segmentSelected {
    [self getOffers];
}


@end
