//
//  ViewController.h
//  SwiftBack
//
//  Created by Hengyu Lim on 9/4/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface ViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
- (IBAction)goToDriver:(id)sender;

- (IBAction)startWalkthrough:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnBackClick;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *movieView;
@property (weak, nonatomic) IBOutlet UIView *gradintView;
@end
